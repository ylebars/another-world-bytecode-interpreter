#!/usr/bin/env sh
# -*- coding: utf-8 -*-

# Creates the build environment for GCC and Ninja on a Unix-like system.

# GCC C++ command.
compiler=$(which g++)

cmake .. -DCMAKE_CXX_COMPILER=$compiler -DCMAKE_BUILD_TYPE=Debug \
    -DCMAKE_CXX_FLAGS_DEBUG="-g -pedantic -Wall -Werror -pipe" \
    -DCMAKE_CXX_FLAGS_MINSIZEREL="-march=native -Os -DNDEBUG -pipe" \
    -DCMAKE_CXX_FLAGS_RELEASE="-march=native -O3 -DNDEBUG -pipe" \
    -DCMAKE_CXX_FLAGS_RELWITHDEBINFO="-march=native -O2 -g -pipe -fno-omit-frame-pointer" \
    -DHTML=TRUE -DFORCE_COLORED_OUTPUT=TRUE -G "Ninja" -B ../build
