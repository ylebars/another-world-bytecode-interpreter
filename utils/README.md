This directory contains scripts to generate compile environment as
recommended by the developing team.

Voluntarily, these scripts are kept quite simple: all they are doing is creating
the directory `build/` and setting up the compile environment. If you want to
use the system package manager on systems such as **GNU/Linux** and __*BSD__,
you should install dependencies before running one of these scripts. If you want
to use Conan, you should have add the Bincrafters repository, but the scripts
install the dependencies. Voluntarily, the scripts do not run the compilation.

Scripts whose name start with “package” are oriented for package creation, while
the other are oriented for development.

On **Unix** like system such as **GNU/Linux** or __*BSD__, you probably should
prefer to use the package manager for dependencies when developing. When making
a package, you probably should prefer distribution package manager for
distribution package, and **Conan** for
[Appimage](https://appimage.org/ "Appimage"),
[Flatpak](https://flatpak.org/ "Flatpak"), or
[Snap](https://snapcraft.io/ "Snap").

To create **macOS** package for **Homebrew**, **MacPorts**, or **Fink**, you
probably should prefer to use these package managers for dependencies. To create
Dmg package, you should probably prefer **Conan**.

Here are the behaviour of the provided scripts:

* _unix-gcc-make.sh_: generates a compile environment based on **GCC** and
**Make** on an **Unix**-like system, and using the system packages manager,
which is the default on **GNU/Linux** for instance;

* _unix-gcc-make-conan.sh_: generates a compile environment based on **GCC** and
**Make**, using **Conan**, on an **Unix**-like system;

* _unix-clang-make.sh_: generates a compile environment based on **CLang** and
**Make** on an **Unix**-like system;

* _unix-clang-make-conan.sh_: generates a compile environment based on **CLang**
and **Make**, using **Conan**, on an **Unix**-like system;

* _unix-gcc-ninja.sh_: generates a compile environment based on **GCC** and
**Ninja** on an **Unix**-like system;

* _unix-gcc-ninja-conan.sh_: generates a compile environment based on **GCC**
and **Ninja**, using **Conan**, on an **Unix**-like system;

* _unix-clang-ninja.sh_: generates a compile environment based on **CLang** and
**Ninja** on an **Unix**-like system;

* _unix-clang-ninja-conan.sh_: generates a compile environment based on
**CLang** and **Ninja**, using **Conan**, on an **Unix**-like system;

* _mingw.sh_: generates a compile environment for **MinGW**;

* _xcode.sh_: generates a compile environment for **Xcode**;

* _xcode-conan.sh_: generates a compile environment for **Xcode**, using
**Conan**;

* _xcode-ninja.sh_: generates a compile environment for **Xcode**;

* _xcode-ninja-conan.sh_: generates a compile environment for **Xcode**, using
**Ninja** and **Conan**;

* _vs.ps1_: generates a compile environment for **Visual studio**;

* _package-unix-gcc-make.sh_: generates a compile environment oriented for
package making, based on **GCC** and **Make** on an **Unix**-like system, and
using the system packages manager;

* _package-unix-gcc-make-conan.sh_: generates a compile environment oriented for
package making, based on **GCC** and **Make**, using **Conan**, on an
**Unix**-like system;

* _package-unix-gcc-ninja.sh_: generates a compile environment oriented for
package making, based on **GCC** and **Ninja** on an **Unix**-like system;

* _package-unix-gcc-ninja-conan.sh_: generates a compile environment oriented
for package making, based on **GCC** and **Ninja**, using **Conan**, on an
**Unix**-like system;

* _package-unix-clang-make.sh_: generates a compile environment oriented for
package making, based on **CLang** and **Make** on an **Unix**-like system;

* _package-unix-clang-make-conan.sh_: generates a compile environment oriented
for package making, based on **CLang** and **Make**, using **Conan**, on an
**Unix**-like system;

* _package-unix-clang-ninja.sh_: generates a compile environment oriented for
package making, based on **CLang** and **Ninja** on an **Unix**-like system;

* _package-unix-clang-ninja-conan.sh_: generates a compile environment oriented
for package making, based on **CLang** and **Ninja**, using **Conan**, on an
**Unix**-like system;

* _package-mingw.sh_: generates a compile environment oriented for package
making for **MinGW**;

* _package-xcode.sh_: generates a compile environment oriented for package
making for **Xcode**;

* _package-xcode-conan.sh_: generates a compile environment oriented for package
making for **Xcode**, using **Conan**;

* _package-xcode-ninja.sh_: generates a compile environment oriented for package
making for **Xcode**, using **Ninja**;

* _package-xcode-ninja-conan.sh_: generates a compile environment oriented for
package making for **Xcode**, using **Ninja** and **Conan**;

* _package-vs.ps1_: generates a compile environment oriented for package making
for **Visual studio**.
