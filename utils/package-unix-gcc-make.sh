#!/usr/bin/env sh
# -*- coding: utf-8 -*-

# Creates a build environment adapted for package making on Unix-like system,
# based on GCC and Make.

# GCC C++ command.
compiler=$(which g++)

cmake .. -DCMAKE_CXX_COMPILER=$compiler -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_CXX_FLAGS_DEBUG="-g -pedantic -Wall -Werror -pipe" \
    -DCMAKE_CXX_FLAGS_MINSIZEREL="-march=native -Os -DNDEBUG -pipe" \
    -DCMAKE_CXX_FLAGS_RELEASE="-march=native -O3 -DNDEBUG -pipe" \
    -DCMAKE_CXX_FLAGS_RELWITHDEBINFO="-march=native -O2 -g -pipe -fno-omit-frame-pointer" \
    -DMAN=TRUE -G "Unix Makefiles" -B ../build
