NEO-RAW
=======

This is an Another World VM implementation.  Based on Gregory Montoir’s
original work, the code-base has been cleaned up with legibility,
readability, portability and performance in mind, using Fabien Sanglard’s work
as a starting point. It has been tested on
[GNU](https://www.gnu.org/ "The GNU project")/[Linux](https://www.kernel.org/ "The Linux Kernel"),
[Microsoft Windows<sup>®</sup>](http://windows.microsoft.com/ "Microsoft Windows"),
[macOS<sup>®</sup>](https://www.apple.com/macos/ "macOS") and
[Rapsbian](https://www.raspbian.org/ "Raspbian").

If you never played to Another World, you can try it on-line:

<https://archive.org/details/msdos_Out_of_This_World_1991>

**Contents**

- [Architecture](#architecture "Architecture")
- [About](#about "About")
- [Supported versions](#supported-versions "Supported versions")
- [Compiling](#compiling "Compiling")
- [Running](#running "Running")
- [Credits](#credits "Credits")
- [Licence](#licence "Licence")

Architecture
------------

[The wiki of the project](https://framagit.org/ylebars/another-world-bytecode-interpreter/-/wikis/ "Project wiki")
gives an extensive analysis on the code architecture.

[Documentation specific to this code is available on-line.](https://ylebars.frama.io/another-world-bytecode-interpreter/ "Code documentation")

About
-----

Raw is a re-implementation of the engine used in the game Another World. This 
game, released under the name Out Of This World in non-European countries, was 
written by
[Éric Chahi](http://www.anotherworld.fr/anotherworld_uk/index.htm "Éric Chahi’s webpage")
at the beginning of the ’90s. More information can be  found here:

http://www.mobygames.com/game/sheet/p,2/gameId,564/

Supported versions
------------------

English Amiga, Atari ST, and MS-DOS versions (“Out of this World”) are
supported.

Compiling
---------

### Dependencies

For compiling this code, you need a C++-2017 compliant compiler,
[SDL2](https://www.libsdl.org/ "SDL2") and
[Boost](https://www.boost.org/ "Boost") (more specifically
Boost.program_options, Boost.format, Boost.filesystem, Boost.endian, and
Boost.algorithm) at least version 1.58.0.

Optionally, to generate the documentation, you will need
[Doxygen](https://www.doxygen.nl/index.html "Doxygen") and
[Graphviz](https://graphviz.org/ "Graphviz").

Depending on your platform of choice, there are several possibilities offered
to manage the dependencies:

* **GNU/Linux** and __*BSD__: dependencies can be managed by the package
manager or by [Conan](https://conan.io/ "Conan, the C/C++ Package Manager");
* **macOS<sup>®</sup>**: dependencies can be managed  with Conan,
[Homebrew](http://brew.sh/ "Homebrew"),
[MacPorts](https://www.macports.org/ "MacPorts"), or
[Fink](https://www.finkproject.org/ "Fink");
* **Microsoft Windows<sup>®</sup>**: only Conan is supported.

### Quick starter

Compiling the code is pretty straightforward. The following will give some
details so you can adapt finely the process to your environment. Anyway, here is
a sum-up on how to compile for the first time this code:

Either install the dependencies listed above, or install Conan and run the
following command:

```Bash
conan remote add bincrafters https://bincrafters.jfrog.io/artifactory/api/conan/public-conan
conan config set general.revisions_enabled=1
```

Then, go in the `utils/` directory, select the building environment generator
that suits your need (the file `utils/README.md` describe the generators) and
run it.

Last step, compile the project with your compiling environment, as you are used
to do!

### Mandatory compilation options

Some compilation options are mandatory to compile this project. Our CMake
structure sets them, so you do not have to worry about them. Still, as an
information, here are those options:

* on **Microsoft Windows<sup>®</sup>** with
[Visual Studio](https://visualstudio.microsoft.com/ "Visual Studio"): `MD`;

* with [GCC](https://gcc.gnu.org/, "GCC") lower than 9.0:
`-lstdc++ -DBOOST_LOG_DYN_LINK`;

* with **GCC** greater than 9.0 and [Clang](https://clang.llvm.org/ "Clang"):
`-DBOOST_LOG_DYN_LINK`.

#### Using Conan

You have to add the “Bincrafters repository” to your Conan installation
and set the correct policy in order to make use of SDL2:
```Bash
conan remote add bincrafters https://bincrafters.jfrog.io/artifactory/api/conan/public-conan
conan config set general.revisions_enabled=1
```

Ask Conan to install the project dependencies:
```Bash
conan install ..
```
**Important note** - GCC user on Linux have to specify the use of newer ABI,
like this:
```Bash
conan install .. --settings compiler.libcxx="libstdc++11"
```

In order for CMake to let Conan to handle the dependencies, set the `USE_CONAN`
variable to `TRUE`. Either by modifiying  *CMakeLists.txt* or by adding
`-DUSE_CONAN=TRUE` when executing `cmake`. More on CMake in the next section.

### Building with CMake

The project is managed using [CMake](https://cmake.org/ "CMake").

A good practice is to build “off sources”. Create a directory
called _build/_ in the main directory. Inside _build/_, let `CMake` generate a
project suitable to your environment, then build.

#### Buiding the executable

In order to build the release binary:

1. Generate the project for the build tool of your environment:

Without relying on Conan to manage the dependencies:
```Bash
cmake .. -DCMAKE_BUILD_TYPE=Release
```
Or relying on Conan to manage the dependencies:
```Bash
cmake .. -DCMAKE_BUILD_TYPE=Release -DUSE_CONAN=TRUE
```

The directory _utils/_ contains several build environment generators to help you
setting up your environment. The file
[_utils/README.md_](utils/README.md "Environment generators documentation")
gives extensive documentation for these generators.

2. Build the executable:

```Bash
cmake --build . --config release
```

The resulting object file will then be located in 
_build/src/bin/_`${COMPILATION_MODE}`_/_, where `${COMPILATION_MODE}` is the
name of the compilation mode used, such as “Debug” or “Release.”

#### Optional steps

If your building tool support it, you can straightly install the binary with
the following command, that may require root privileges:
```Bash
cmake --build . --config release --target install
```

On supporting platforms, you can also create a package, using CPack. For
instance:

```Bash
cpack -C "Release" -G "STGZ;TGZ"
```

The build system can generate documentation in the following formats: HTML,
DocSet ([Xcode<sup>®</sup>](https://developer.apple.com/xcode/ "Xcode")
documentation format), HTMLHelp (Visual Studio<sup>®</sup> documentation
format), EclipseHelp ([Eclipse](https://www.eclipse.org/ "Eclipse")
documentation format), XML, DocBook,
[LaTeX](https://www.latex-project.org/ "LaTeX"), and Manpage. To generate HTML
documentation, add `-DHTML=TRUE` to CMake command; to generate DocSet
documentation, add `-DDOCSET=TRUE` to CMake command; to generate HTMLHelp
documentation, add `-DHTMLHELP=TRUE` to CMake command; to generate EclipseHelp
documentation, add `-DECLIPSEHELP=TRUE` to CMake command; to generate XML
documentation, add `-DXML=TRUE` to CMake command; to generate DocBook
documentation, add `-DDOCBOOK=TRUE` to CMake command; to generate LaTeX
documentation, add `-DLATEX=TRUE` to CMake command; to generate Manpage
documentation, add `-DMAN=TRUE` to CMake command.

To create documentation, simply use the “doc” entry in your compilation
files. This require the presence of `doxygen`in your `PATH` environment
variable.

```Bash
cmake --build . --target doc
```

Resulting documentations will be in _doc/_.

If `HTML`, `DOCSET`, `HTMLHELP`, `ECLIPSEHELP`, `XML`, `DOCBOOK`, `LATEX`, and
`MAN` are set to `FALSE` then documentation will be generated in all supported
formats.

Of course, types of compilation can be changed using CMake, see CMake
documentation for more details.

#### Suggested options

If the compiler you are using is GCC or [Clang](https://clang.llvm.org/ "Clang")
on a x86_64 architecture, we suggest you set the following variables (adapt
this to your compiler and architecture if needed):

```Text
//Flags to be used by the compiler during debug builds.
CMAKE_CXX_FLAGS_DEBUG:STRING=-g -pedantic -Wall -Werror -pipe

//Flags to be used by the compiler during release minsize builds.
CMAKE_CXX_FLAGS_MINSIZEREL:STRING=-march=native -Os -DNDEBUG -pipe

//Flags to be used by the compiler during release builds.
CMAKE_CXX_FLAGS_RELEASE:STRING=-march=native -O3 -DNDEBUG -pipe

//Flags to be used by the compiler during Release with Debug Info builds.
CMAKE_CXX_FLAGS_RELWITHDEBINFO:STRING=-march=native -O2 -g -pipe -fno-omit-frame-pointer
```

Running
-------

Here is the command line prototype:

    raw [Options]

Supported options are:

    -h [ --help ]              Display this help message.
    -v [ --version ]           Display program version.
    -d [ --datapath ] arg (=.) Path to game data.
    -m [ --mute ]              Mute the sound.
    -c [ --debugConsole ]      Enable logs through the standard output.
    -f [ --debugFile ] arg     Enable logs into a file.
    -s [ --debugSyslog ] arg   Enable logs through Syslog.
    -l [ --level ] arg         Log level filter.

To obtain some help, simply type:

```Bash
./raw --help
```

In order to run the game, you will need the original files. You can either:

* put the data files of the game in the same directory than the executable, the
icon image (`logo-aw.bmp`), and the game controllers calibration file
(`gamecontrollerdb.txt`);
* use the `--datapath` command line option to specify the data files directory
(this is the recommended way).

The icon image (`logo-aw.bmp`) as well as the game controllers calibration file
(`gamecontrollerdb.txt`) should always be located in the same directory
than the executable.

The available log levels are:

* debug
* information
* warning
* error

The higher level also trigger on the lower levels. For instance, if you ask for
“warning” level, you will also get “error” level. Default is “error”.

Here are the various in game hotkeys:

* \<Arrow Keys\>: allow you to move Lester
* \<Enter\>/\<Space\>: allow you run/shoot with your gun
* \<C\>: allow to enter a code to jump at a specific level
* \<P\>: pause the game
* \<Esc\>/\<Q\>: exit the game
* \<Alt\> + \<+\> and \<Alt\> + \<-\>: change window scale factor
* \<F\>: toggle full-screen mode
* \<S\>: toggle sound off and on
* \<+\> and \<-\>: increase and decrease sound level

Credits
-------

Éric Chahi, obviously, for making this great game.

Licence
-------

This software is distributed under GNU GPLv2 licence. It can be
copied and modified freely as long as initial authors are cited and the
license is kept the same. Complete text of the licence is in
[`LICENSE.txt`](LICENSE.txt "GNU GPLv2 licence") file and can be found
[on-line](https://choosealicense.com/licenses/gpl-2.0/ "GNU GPLv2").

POSIX is a registered trademark owned by
[the Open Group](http://opengroup.org/ "The Open Group").

Linux is a registered trademark owned by
[Torvalds, Linus](https://github.com/torvalds "Torvalds, Linus on Github.").

macOS and Xcode are registered trademarks owned by
[Apple](https://www.apple.com/ "Apple").

Microsoft Windows and Visual studio are registered trademarks owned by
[Microsoft](https://www.microsoft.com/ "Microsoft").

Copyright © 2004 – 2021.
