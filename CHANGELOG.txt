== Version 2.4 ==

Switching the whole log system to SDL.

== Version 2.3 ==

Adding exceptions structure.

== Version 2.2 ==

Adding support for Amiga and Atari ST version.

Adding log gestion using SDL.

== Version 2.1 ==

Suppressing saving and loading of game-states, as it was in contradiction with
the gameplay.

Using Boost.log for debug log.

== Version 2.0 ==

Cleaning code and creating Doxygen documentation.

Improving CMake structure and adding Conan package management.
