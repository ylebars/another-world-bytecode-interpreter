#ifndef __ERRORS_HPP__
#define __ERRORS_HPP__

/**
 * \file errors.hpp
 * \brief Classes for error handling.
 * \author Yoann LE BARS
 */

#include <exception>
#include <string>

namespace AnotherWorld {
    /// \brief Mother class for errors handling.
    class AWError: public std::exception {
        public:
            /// \brief Default constructor.
            AWError () throw () {};

            /**
             * \brief Copy constructor.
             * \param other Object to be copied.
             */
            AWError (const AWError &other) throw () {}

            /**
             * \brief Move constructor.
             * \param other Object to be moved.
             */
            AWError (AWError &&other) throw () {}

            /// \brief Destructor.
            virtual ~AWError () throw () {};

            /**
             * \brief Copy assignment operator.
             * \param other Object to be copied.
             */
            AWError &operator = (const AWError &other) throw () {return *this;}

            /**
             * \brief Move assignment operator.
             * \param other Object to be copied.
             */
            AWError &operator = (AWError &&other) throw () {return *this;}

            /**
             * \brief Generates the error message.
             * \returns The error message.
             */
            virtual const char* what () const throw () override {
                return "An error occurred.";
            }
    };

    /// \brief Class handling errors when trying to locate game data file.
    class UnlocatableFile: public AWError {
        public:
            /**
             * \brief Constructor.
             * \param _fileName Name of the file on which the error occurred.
             */
            explicit UnlocatableFile (const std::string &_fileName) throw ():
                    fileName_ (_fileName) {}

            /**
             * \brief Copy constructor.
             * \param other Object to be copied.
             */
            UnlocatableFile (const UnlocatableFile &other) throw ():
                    fileName_ (other.fileName_) {}

            /**
             * \brief Move constructor.
             * \param other Object to be moved.
             */
            UnlocatableFile (UnlocatableFile &&other) throw ():
                    fileName_ (other.fileName_) {other.fileName_.clear();}

            /// \brief Destructor.
            virtual ~UnlocatableFile () throw () {};

            /**
             * \brief Copy assignment operator.
             * \param other Object to be copied.
             */
            UnlocatableFile &operator = (const UnlocatableFile &other)
                    throw () {
                fileName_ = other.fileName_;
                return *this;
            }

            /**
             * \brief Move assignment operator.
             * \param other Object to be copied.
             */
            UnlocatableFile &operator = (UnlocatableFile &&other) throw () {
                fileName_ = other.fileName_;
                other.fileName_.clear();
                return *this;
            }

            /**
             * \brief Access to file name on which the error occurred.
             * \returns The file name.
             */
            std::string fileName () const throw () {return fileName_;}

            /**
             * \brief Generates the error message.
             * \returns The error message.
             */
            virtual const char* what () const throw () override;

        private:
            /// \brief Name of the file that cannot be located.
            std::string fileName_;
    };

    /// \brief Class handling errors when trying to read data list entries.
    class MemoryReadingError: public AWError {
        public:
            /**
             * \brief Constructor.
             * \param _filePath Path to the file on which the error occurred.
             */
            explicit MemoryReadingError (const std::string &_filePath) throw ():
                    filePath_ (_filePath) {}

            /**
             * \brief Copy constructor.
             * \param other Object to be copied.
             */
            MemoryReadingError (const MemoryReadingError &other) throw ():
                    filePath_ (other.filePath_) {}

            /**
             * \brief Move constructor.
             * \param other Object to be moved.
             */
            MemoryReadingError (MemoryReadingError &&other) throw ():
                    filePath_ (other.filePath_) {other.filePath_.clear();}

            /// \brief Destructor.
            virtual ~MemoryReadingError () throw () {};

            /**
             * \brief Copy assignment operator.
             * \param other Object to be copied.
             */
            MemoryReadingError &operator = (const MemoryReadingError &other)
                    throw () {
                filePath_ = other.filePath_;
                return *this;
            }

            /**
             * \brief Move assignment operator.
             * \param other Object to be copied.
             */
            MemoryReadingError &operator = (MemoryReadingError &&other)
                    throw () {
                filePath_ = other.filePath_;
                other.filePath_.clear();
                return *this;
            }

            /**
             * \brief Access to file name on which the error occurred.
             * \returns The file name.
             */
            std::string filePath () const throw () {return filePath_;}

            /**
             * \brief Generates the error message.
             * \returns The error message.
             */
            virtual const char* what () const throw () override;

        private:
            /// \brief Name of the file that cannot be located.
            std::string filePath_;
    };

    /// \brief Class handling errors when trying to find game signature.
    class FailedSignatureReading: public AWError {
        public:
            /**
             * \brief Constructor.
             * \param _fileName Name of the file on which the error occurred.
             */
            explicit FailedSignatureReading (const std::string &_fileName)
                    throw (): fileName_ (_fileName) {}

            /**
             * \brief Copy constructor.
             * \param other Object to be copied.
             */
            FailedSignatureReading (const FailedSignatureReading &other)
                    throw (): fileName_ (other.fileName_) {}

            /**
             * \brief Move constructor.
             * \param other Object to be moved.
             */
            FailedSignatureReading (FailedSignatureReading &&other) throw ():
                    fileName_ (other.fileName_) {other.fileName_.clear();}

            /// \brief Destructor.
            virtual ~FailedSignatureReading () throw () {};

            /**
             * \brief Copy assignment operator.
             * \param other Object to be copied.
             */
            FailedSignatureReading &operator =
                    (const FailedSignatureReading &other) throw () {
                fileName_ = other.fileName_;
                return *this;
            }

            /**
             * \brief Move assignment operator.
             * \param other Object to be copied.
             */
            FailedSignatureReading &operator =
                    (FailedSignatureReading &&other) throw () {
                fileName_ = other.fileName_;
                other.fileName_.clear();
                return *this;
            }

            /**
             * \brief Access to file name on which the error occurred.
             * \returns The file name.
             */
            std::string fileName () const throw () {return fileName_;}

            /**
             * \brief Generates the error message.
             * \returns The error message.
             */
            virtual const char* what () const throw () override;

        private:
            /// \brief Name of the file that cannot be located.
            std::string fileName_;
    };

    /// \brief Exception when failing to find a valid game signature.
    class NoValidSignature: public AWError {
        public:
            /**
             * \brief Constructor.
             * \param _fileName Name of the file on which the error occurred.
             */
            explicit NoValidSignature (const std::string &_fileName)
                    throw (): fileName_ (_fileName) {}

            /**
             * \brief Copy constructor.
             * \param other Object to be copied.
             */
            NoValidSignature (const NoValidSignature &other)
                    throw (): fileName_ (other.fileName_) {}

            /**
             * \brief Move constructor.
             * \param other Object to be moved.
             */
            NoValidSignature (NoValidSignature &&other) throw ():
                    fileName_ (other.fileName_) {other.fileName_.clear();}

            /// \brief Destructor.
            virtual ~NoValidSignature () throw () {};

            /**
             * \brief Copy assignment operator.
             * \param other Object to be copied.
             */
            NoValidSignature &operator = (const NoValidSignature &other)
                    throw () {
                fileName_ = other.fileName_;
                return *this;
            }

            /**
             * \brief Move assignment operator.
             * \param other Object to be copied.
             */
            NoValidSignature &operator = (NoValidSignature &&other) throw () {
                fileName_ = other.fileName_;
                other.fileName_.clear();
                return *this;
            }

            /**
             * \brief Access to file name on which the error occurred.
             * \returns The file name.
             */
            std::string fileName () const throw () {return fileName_;}

            /**
             * \brief Generates the error message.
             * \returns The error message.
             */
            virtual const char* what () const throw () override;

        private:
            /// \brief Name of the file that cannot be located.
            std::string fileName_;
    };

    /// \brief Exception when found no known data type.
    class UnsupportedDataType: public AWError {
        public:
            /// \brief Default constructor.
            UnsupportedDataType () throw () {};

            /**
             * \brief Copy constructor.
             * \param other Object to be copied.
             */
            UnsupportedDataType (const UnsupportedDataType &other) throw () {}

            /**
             * \brief Move constructor.
             * \param other Object to be moved.
             */
            UnsupportedDataType (UnsupportedDataType &&other) throw () {}

            /// \brief Destructor.
            virtual ~UnsupportedDataType () throw () {};

            /**
             * \brief Copy assignment operator.
             * \param other Object to be copied.
             */
            UnsupportedDataType &operator = (const UnsupportedDataType &other)
                    throw () {return *this;}

            /**
             * \brief Move assignment operator.
             * \param other Object to be copied.
             */
            UnsupportedDataType &operator = (UnsupportedDataType &&other)
                    throw () {return *this;}

            /**
             * \brief Generates the error message.
             * \returns The error message.
             */
            virtual const char* what () const throw () override {
                return "Either an invalid directory for data files or an unsupported data type.";
            }
    };

    /// \brief Exception when unable to initialise log file.
    class UninitialisedLogFile: public AWError {
        public:
            /**
             * \brief Constructor.
             * \param _message Error message.
             */
            explicit UninitialisedLogFile (const std::string &_message)
                    throw (): message (_message) {}

            /**
             * \brief Copy constructor.
             * \param other Object to be copied.
             */
            UninitialisedLogFile (const UninitialisedLogFile &other)
                    throw (): message (other.message) {}

            /**
             * \brief Move constructor.
             * \param other Object to be moved.
             */
            UninitialisedLogFile (UninitialisedLogFile &&other) throw ():
                    message (other.message) {other.message.clear();}

            /// \brief Destructor.
            virtual ~UninitialisedLogFile () throw () {};

            /**
             * \brief Copy assignment operator.
             * \param other Object to be copied.
             */
            UninitialisedLogFile &operator = (const UninitialisedLogFile &other)
                    throw () {
                message = other.message;
                return *this;
            }

            /**
             * \brief Move assignment operator.
             * \param other Object to be copied.
             */
            UninitialisedLogFile &operator = (UninitialisedLogFile &&other)
                    throw () {
                message = other.message;
                other.message.clear();
                return *this;
            }

            /**
             * \brief Generates the error message.
             * \returns The error message.
             */
            virtual const char* what () const throw () override {
                return message.c_str();
            }

        private:
            /// \brief Name of the file that cannot be located.
            std::string message;
    };

    /**
     * \brief Exception when trying to initialise an already initialised log
     * file.
     */
    class AlreadyInitialisedLogFile: public AWError {
        public:
            /**
             * \brief Constructor.
             * \param _message Error message.
             */
            explicit AlreadyInitialisedLogFile (const std::string &_message)
                    throw (): message (_message) {}

            /**
             * \brief Copy constructor.
             * \param other Object to be copied.
             */
            AlreadyInitialisedLogFile (const AlreadyInitialisedLogFile &other)
                    throw (): message (other.message) {}

            /**
             * \brief Move constructor.
             * \param other Object to be moved.
             */
            AlreadyInitialisedLogFile (AlreadyInitialisedLogFile &&other) throw ():
                    message (other.message) {other.message.clear();}

            /// \brief Destructor.
            virtual ~AlreadyInitialisedLogFile () throw () {};

            /**
             * \brief Copy assignment operator.
             * \param other Object to be copied.
             */
            AlreadyInitialisedLogFile &operator = (const AlreadyInitialisedLogFile &other)
                    throw () {
                message = other.message;
                return *this;
            }

            /**
             * \brief Move assignment operator.
             * \param other Object to be copied.
             */
            AlreadyInitialisedLogFile &operator = (AlreadyInitialisedLogFile &&other)
                    throw () {
                message = other.message;
                other.message.clear();
                return *this;
            }

            /**
             * \brief Generates the error message.
             * \returns The error message.
             */
            virtual const char* what () const throw () override {
                return message.c_str();
            }

        private:
            /// \brief Name of the file that cannot be located.
            std::string message;
    };

    /// \brief Exception when asked for an invalid severity level.
    class InvalidSeverityLevel: public AWError {
        public:
            /**
             * \brief Constructor.
             * \param _message Error message.
             */
            explicit InvalidSeverityLevel (const std::string &_message)
                    throw (): message (_message) {}

            /**
             * \brief Copy constructor.
             * \param other Object to be copied.
             */
            InvalidSeverityLevel (const InvalidSeverityLevel &other)
                    throw (): message (other.message) {}

            /**
             * \brief Move constructor.
             * \param other Object to be moved.
             */
            InvalidSeverityLevel (InvalidSeverityLevel &&other) throw ():
                    message (other.message) {other.message.clear();}

            /// \brief Destructor.
            virtual ~InvalidSeverityLevel () throw () {};

            /**
             * \brief Copy assignment operator.
             * \param other Object to be copied.
             */
            InvalidSeverityLevel &operator = (const InvalidSeverityLevel &other)
                    throw () {
                message = other.message;
                return *this;
            }

            /**
             * \brief Move assignment operator.
             * \param other Object to be copied.
             */
            InvalidSeverityLevel &operator = (InvalidSeverityLevel &&other)
                    throw () {
                message = other.message;
                other.message.clear();
                return *this;
            }

            /**
             * \brief Generates the error message.
             * \returns The error message.
             */
            virtual const char* what () const throw () override {
                return message.c_str();
            }

        private:
            /// \brief Name of the file that cannot be located.
            std::string message;
    };

    /// \brief Exception when an unrecoverable error occurred.
    class UnrecoverableError: public AWError {
        public:
            /**
             * \brief Constructor.
             * \param _message Error message.
             */
            explicit UnrecoverableError (const std::string &_message)
                    throw (): message (_message) {}

            /**
             * \brief Copy constructor.
             * \param other Object to be copied.
             */
            UnrecoverableError (const UnrecoverableError &other)
                    throw (): message (other.message) {}

            /**
             * \brief Move constructor.
             * \param other Object to be moved.
             */
            UnrecoverableError (UnrecoverableError &&other) throw ():
                    message (other.message) {other.message.clear();}

            /// \brief Destructor.
            virtual ~UnrecoverableError () throw () {};

            /**
             * \brief Copy assignment operator.
             * \param other Object to be copied.
             */
            UnrecoverableError &operator = (const UnrecoverableError &other)
                    throw () {
                message = other.message;
                return *this;
            }

            /**
             * \brief Move assignment operator.
             * \param other Object to be copied.
             */
            UnrecoverableError &operator = (UnrecoverableError &&other)
                    throw () {
                message = other.message;
                other.message.clear();
                return *this;
            }

            /**
             * \brief Generates the error message.
             * \returns The error message.
             */
            virtual const char* what () const throw () override {
                return message.c_str();
            }

        private:
            /// \brief Name of the file that cannot be located.
            std::string message;
    };
}

#endif
