#ifndef __ENGINE_HPP__
#define __ENGINE_HPP__

/**
 * \file engine.hpp
 * \brief Game engine definition: the object which drives the game.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \author willl
 * \author Glaize, Sylvain
 */

#include <string>

#include "mixer.hpp"
#include "resources.hpp"
#include "sfxplayer.hpp"
#include "video.hpp"
#include "vm.hpp"
#include "sys.hpp"
#include "parts.hpp"

namespace AnotherWorld {
    /// \brief Definition of game engine.
    class Engine {
        public:
            /**
             * \brief Construct the engine with system description and the path
             * where to find game assets.
             * \param paramSys System parameters.
             * \param _dataDir Path to the directory where to find game assets.
             * \param volume Asked volume.
             */
            Engine (SDLStub* paramSys, const std::string &_dataDir,
                    uint8_t volume);

            /**
             * \brief Running the game.
             * \returns 0 if everything goes well,
             *     -8 if an error occurred while reading memory list,
             *     -12 if an unrecoverable error occur when running the game.
             */
            int run ();

        private:
            /// Directory where to find game assets.
            std::string dataDir;

            /// System description.
            SDLStub* sys;

            /// Object to mix game sound.
            Mixer mixer;

            /// System resources to be used into the game.
            Resource res;

            /// Object for video window.
            Video video;

            /// Object for audio Sfx.
            SfxPlayer player;

            /// Detection system for the game data version.
            TitleDetection detection;

            /// Virtual machine that runs game byte-code.
            VirtualMachine vm;
    };
}

#endif
