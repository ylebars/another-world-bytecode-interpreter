#ifndef __RESOURCES_HPP__
#define __RESOURCES_HPP__

/**
 * \file resources.hpp
 * \brief Definitions to manage game assets.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \author willll
 * \author Glaize, Sylvain
 */

#include <array>
#include <cstdint>

#include "endian.hpp"
#include "parts.hpp"
#include "titleDetector.hpp"
#include "vmMemoryAllocator.hpp"

namespace AnotherWorld {
    // Forward declaration.
    class Video;

    /// \brief Describe resource state.
    struct MemState {
        /// \brief Resource possible state.
        enum : uint8_t {
            STATE_NOT_NEEDED = 0,
            STATE_LOADED = 1,
            STATE_LOAD_ME = 2,
            STATE_END_OF_MEMLIST = 0xFF
        };

        /// \brief Maximum number of ressources.
        static const auto MemStateArraySize = 150;
    };

    /**
     * \brief Handling assets files.
     *
     * This is a directory entry. When the game starts, it loads memlist.bin and
     * populate an array of MemEntry
     */
    struct MemEntry {
        /**
         * \brief Entry state.
         *
         * This is not a boolean, it can have value 0, 1, 2 or 255,
         * respectively meaning:
         *     0:NOT_NEEDED 1:LOADED 2:LOAD_ME 255:END_OF_MEMLIST
         * See MemState enum.
         */
        std::uint8_t state;        // 0x0

        /// \brief Resource type.
        std::uint8_t type;         // 0x1, Resource::ResType

        /// \brief Pointer to actual data in memory once loaded.
        std::uint8_t *bufPtr;      // 0x2

        /// \brief To complete ...
        std::uint8_t rankNum;      // 0x6

        /// \brief Bank identifier.
        std::uint8_t bankId;       // 0x7

        /// \brief Offset of the resource in the bank file.
        std::uint32_t bankOffset;  // 0x8

        /**
         * \brief Size of data when packed.
         *
         * All ressources are packed (for a gain of 28 % according to Chahi)
         */
        std::uint32_t packedSize;  // 0xC

        /// \brief Size of data when unpacked.
        std::uint32_t size;   // 0x12

        /// \brief To which endianness does comply the file.
        Endian fileEndianness;

        /// \brief Tells if this resource is packed in the bank or not.
        [[nodiscard]] bool isPacked() const {return size != packedSize;}
    };

    /// \brief Handling game assets.
    class Resource {
        public:
            /// \brief Describe the kind of resource being handled.
            enum ResType {
                /// \brief Sound effect.
                RT_SOUND = 0,

                /// \brief Music.
                RT_MUSIC = 1,

                /**
                 * \brief Cut scene.
                 *
                 * full screen video buffer, size=0x7D00
                 */
                RT_POLY_ANIM = 2,

                /* FCS: 0x7D00=32000...but 320x200 = 64000 ??
                 * Since the game is 16 colors, two pixels palette indices can
                 * be stored in one byte that's why we can store two pixels
                 * palette indices in one byte and we only need 320*200/2 bytes
                 * for an entire screen. */

                /**
                 * \brief Current palette.
                 *
                 * (1024=vga + 1024=ega), size=2048
                 */
                RT_PALETTE = 3,

                /// \brief Script.
                RT_BYTECODE = 4,

                /// \brief To be completed ...
                RT_POLY_CINEMATIC = 5
            };

            /// \brief Memory state of assets.
            std::array<MemEntry, MemState::MemStateArraySize> memList {};

            /// \brief Part of the game to come.
            Game::PartIdentifier requestedNextPart;

            /**
             * \brief Constructor.
             * \param vid Video descriptor.
             * \param _dataDir Directory where to find assets files.
             */
            Resource (Video* vid, const char* _dataDir):
                    requestedNextPart(Game::INVALID_PART),
                    video(vid), dataDir(_dataDir), resourceCount(0),
                    currentPartId_(Game::INVALID_PART),
                    segPalettes_(nullptr), segBytecode_(nullptr),
                    segCinematic_(nullptr), segVideo2(nullptr) {}

            /**
             * \brief Access to current part identifier.
             * \returns The identifier.
             */
            [[nodiscard]] Game::PartIdentifier currentPartId () const {
                return currentPartId_;
            }

            /**
             * \brief Access to palettes location.
             * \returns A pointer to the location.
             */
            [[nodiscard]] std::uint8_t* segPalettes () const {
                return segPalettes_;
            }

            /**
             * \brief Access to bytecode location.
             * \returns A pointer to the location.
             */
            [[nodiscard]] std::uint8_t* segBytecode () const {
                return segBytecode_;
            }

            /**
             * \brief Access to cinematic location.
             * \returns A pointer to the location.
             */
            [[nodiscard]] std::uint8_t* segCinematic () const {
                return segCinematic_;
            }

            /**
             * \brief Read all entries from memlist.bin.
             *
             * Do not load any actual resource data in memory, this is just
             * a fast way to access the data later based on their id.
             */
            void readEntries (const TitleDetection &detection);

            /// \brief Handling invalid entry.
            void invalidateRes ();

            /**
             * \brief Load memory a segment or a resource.
             * \param num Element identifier.
             *
             * This method serves two purpose:
             * - Load parts in memory segments (palette,code,video1,video2);
             *          or
             * - Load a resource in memory.
             *
             * This is decided based on the resourceId. If it does not match a
             * mementry id it is supposed to be a part id.
             */
            void loadPartsOrMemoryEntry (std::uint16_t num);

            /**
             * \brief Set up a given part of the game.
             * \param ptrId Part identifier.
             *
             * Protection screen and cinematic don't need the player and enemies
             * polygon data so memList_[video2Index] is never loaded for those
             * parts of the game. When needed (for action phrases)
             * memList_[video2Index] is always loaded with 0x11 (as seen in
             * memListParts).
             */
            void setupPart (Game::PartIdentifier ptrId);

            /**
             * \brief The two kinds of Video Buffers that are used.
             *
             * Naming still clumsy, as the full usage has yet to be grasped.
             */
            enum VideoSegmentKind {
                USE_VIDEO_SEGMENT,
                USE_CINEMATIC_SEGMENT,
            };

            /**
             * \brief Sets the usage of Video Segment, which is still unclear.
             * \param type Which kind of video segment.
             */
            void setVideoSegmentKind (VideoSegmentKind type) {
                videoSegmentKind = type;
            }

            /**
             * \brief Gets the current pointer for the video data, dependant on the
             * Video Segment usage.
             * \returns The pointer.
             */
            [[nodiscard]] std::uint8_t* getVideoBuffer () const;

        private:
            /// \brief Video descriptor.
            Video* video;

            /// \brief Directory where to find assets files.
            const char* dataDir;

            /// \brief Number of resources in the data.
            std::uint16_t resourceCount;

            /// \brief Current part of the game.
            Game::PartIdentifier currentPartId_;

            /// \brief Segment where to find palettes.
            std::uint8_t* segPalettes_;

            /// \brief Segment where to find byte code.
            std::uint8_t* segBytecode_;

            /// \brief Segment where to find cinematic.
            std::uint8_t* segCinematic_;

            /// \brief Video segment.
            std::uint8_t* segVideo2;

            /// Memory from the virtual machine.
            VMMemoryAllocator memoryAllocator;

            /// What kind of cut scene.
            VideoSegmentKind videoSegmentKind {USE_CINEMATIC_SEGMENT};

            /**
             * \brief Setting up logger for the given part.
             * \param partId Game part identifier.
             * \param gamePartIndices Resources indices for the given part.
             */
            void logPartSetup (Game::PartIdentifier partId,
                               const Parts::PartResourceIndices &gamePartIndices);

            /**
             * \brief Read an entry described in a MemEntry from a Bank file into
             * memory.
             * \param me Game data descriptor.
             * \param dstBuf Distance in the file.
             */
            void readBank (const MemEntry* me, std::uint8_t* dstBuf) const;

            /**
             * \brief Load needed resources.
             *
             * Go over every resource and check if they are marked at
             * "STATE_LOAD_ME". Otherwise, load them in memory and mark them are
             * STATE_LOADED.
             */
            void loadMarkedResourcesAsNeeded ();

            /// \brief Mark all resources from an entry as unneeded.
            void invalidateAll ();

            /**
             * \brief Log loading state.
             * \param me Resource describer.
             * \param loadDestination Where data are loaded.
             */
            void logEntryLoad (const MemEntry* me,
                               std::uint8_t* loadDestination) const;

            /**
             * \brief Determine which is the next entry to be loaded into
             * memory.
             * \returns A pointer to the next entry.
             */
            MemEntry* getNextEntryToLoad ();

            /**
             * \brief Get resource from memory.
             * \param me Memory describer.
             */
            void loadResource (MemEntry* me);
    };

    /**
     * \brief Converts a resource type to a string describing it.
     * \param type Data type identifier.
     */
    const char* resTypeToString (unsigned int type);
}

#endif
