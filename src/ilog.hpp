#ifndef __ILOG_HPP__
#define __ILOG_HPP__

/**
 * \file ilog.hpp
 * \brief Interface for logs.
 * \author Willll
 */

#include <string>
#include <cstdint>
#include <unordered_map>

namespace AnotherWorld {
    /// \brief String describing debug log levels.
    const char* const DebugSTR       = "debug";
    /// \brief String describing information log levels.
    const char* const InformationSTR = "information";
    /// \brief String describing warning log levels.
    const char* const WarningSTR     = "warning";
    /// \brief String describing error log levels.
    const char* const ErrorSTR       = "error";
    /// \brief Empty string utilitarian for describing log levels.
    const char* const EmptySTR       = "";

    /// \brief Describe logging level.
    enum SeverityLevels {
        DEBUG = 1,
        INFORMATION,
        WARNING,
        ERROR,
        DISABLE,     // Must be last to disable logs
        COUNT        // Enum element counter
    };

    /// \brief Describe log levels.
    static const char * const severityLevelsSTR [] = {
        DebugSTR,
        InformationSTR,
        WarningSTR,
        ErrorSTR,
        EmptySTR, EmptySTR
    };

    /// \brief Correspondence between debug level and and debug string.
    static std::unordered_map<std::string, SeverityLevels> const severityLevelsTable = {
        {DebugSTR,         SeverityLevels::DEBUG},
        {InformationSTR,   SeverityLevels::INFORMATION},
        {WarningSTR,       SeverityLevels::WARNING},
        {ErrorSTR,         SeverityLevels::ERROR}
    };

    /**
     * \brief Converts a string into a Logger::SeverityLevels.
     * \param SeverityLevelSTR String to convert into a Logger::SeverityLevels.
     * \returns a Logger::SeverityLevels, DISABLE if not found.
     */
    inline SeverityLevels to_SeverityLevel (const std::string &SeverityLevelSTR) {
        /// Localisation of the severity level in the table.
        auto it = severityLevelsTable.find(SeverityLevelSTR);
        if (it != severityLevelsTable.end()) {
            return it->second;
        } else {
            /* SeverityLevelSTR is not recognized, DISABLE is returned. */
            return SeverityLevels::DISABLE;
        }
    }

    /**
     * \brief Converts Logger::SeverityLevels into a stream.
     * \param strm Stream to write Logger::SeverityLevels into.
     * \param lvl Severity level.
     * \returns Logger::SeverityLevels converted to stream.
     */
    template<typename CharT, typename TraitsT>
    inline std::basic_ostream<CharT, TraitsT> &operator << (
            std::basic_ostream<CharT, TraitsT> &strm, SeverityLevels lvl) {
        static_assert(SeverityLevels::COUNT == std::size(severityLevelsSTR));

        strm << severityLevelsSTR[lvl - 1];
        return strm;
    }

    /**
     * \brief Converts Logger::SeverityLevels into a char *.
     * \param lvl Logger::SeverityLevels into.
     * \returns char * associated to the Logger::SeverityLevels.
     */
    inline const char * toString (const SeverityLevels lvl) {
        static_assert(SeverityLevels::COUNT == std::size(severityLevelsSTR));

        return  severityLevelsSTR[lvl - 1];
    }

    /// \brief Helper class for logger configuration
    class LoggerConfiguration {
        public:
            /**
             * \brief Default constructor.
             * \param _isConsole Whether or not sending logs into the console.
             * \param _isFile Whether or not sending logs into a file.
             * \param _isSyslog Whether or not creating logs.
             * \param _severityLevel Error log severity level.
             * \param _logFile Path to the file where to send logs.
             * \param _ip IP of the device sending the logs.
             * \param _port Port where to send logs.
             */
            LoggerConfiguration(bool _isConsole = false, bool _isFile = false,
                                bool _isSyslog = false,
                                SeverityLevels _severityLevel = SeverityLevels::DISABLE,
                                const std::string &_logFile = "",
                                const std::string &_ip = "",
                                std::uint16_t _port = 514):
                isConsole (_isConsole), isFile (_isFile), isSyslog (_isSyslog),
                severityLevel(_severityLevel), logFile (_logFile), ip (_ip),
                port (_port) {}

            /**
             * \brief Indicates in console logging status.
             * \returns Whether or not sending logs into the console.
             */
            bool isConsoleEnable () const {return isConsole;}

            /**
             * \brief Indicates in file logging status.
             * \returns Whether or not sending logs into a file.
             */
            bool isFileEnable () const {return isFile;}

            /**
            * \brief Indicates in file logging status.
            * \returns Whether or not sending logs into a file.
            */
            void resetFileEnable () {isFile = false;}

            /**
             * \brief Indicates logging status.
             * \returns Whether or not sending logs.
             */
            bool isSyslogEnable () const {return isSyslog;}

            /**
            * \brief Access.
            * \returns Whether or not sending logs.
            */
            SeverityLevels getSeverityLevel () const {return severityLevel;}

            /**
             * \brief Accessing to the path of the log file.
             * \returns The path to the file.
             */
            std::string getLogFilename () const {return logFile;}

            /**
             * \brief Accessing to the the IP of the device sending the logs.
             * \returns The IP.
             */
            std::string getLogIP () const {return ip;}

            /**
             * \brief Accessing to the the port where listening to the logs.
             * \returns The port.
             */
            std::uint16_t getLogPort () const {return port;}

            /**
             * \brief Indicates if logging is enable.
             * \returns Whether or not sending logs.
             */
            bool isLogEnable () const {return isConsole || isFile || isSyslog;}

        private:
            /// \brief Whether or not sending logs into the console.
            bool isConsole;

            /// \brief Whether or not sending logs into a file.
            bool isFile;

            /// \brief Whether or not creating syslog logs.
            bool isSyslog;

            /// \brief logs severity level.
            SeverityLevels severityLevel;

            /// \brief Path to the file where to send logs.
            std::string logFile;

            /// \brief IP of the device sending the logs.
            std::string ip;

            /// \brief Port where to send logs.
            std::uint16_t port;
    };

    /// \brief Interface for logging.
    class ILogger {
        public :
            /// \brief Describing which part of the code to log.
            enum DebugSource : std::uint8_t {
                DBG_VM = 1 << 0,
                DBG_BANK = 1 << 1,
                DBG_VIDEO = 1 << 2,
                DBG_SND = 1 << 3,
                DBG_SER = 1 << 4,
                DBG_INFO = 1 << 5,
                DBG_RES = 1 << 6
            };

            /// \brief destructor
            virtual ~ILogger () {}

            /**
             * \brief Sending a log message.
             * \param slvl Message severity level.
             * \param format Message format describer.
             */
            virtual void log (SeverityLevels slvl, const char* format, ...) = 0;

            /**
             * \brief Sending a debug message.
             * \param cm Code for message to generate.
             * \param format Message format describer.
             */
            virtual void debug (DebugSource cm, const char *format, ...) = 0;

            /**
            * \brief Sending an information message.
            * \param format Message format describer.
            */
            virtual void information (const char* format, ...) = 0;

            /**
             * \brief Sending a warning message.
             * \param format Message format describer.
             */
            virtual void warning (const char* format, ...) = 0;

            /**
             * \brief Sending an error message.
             * \param format Message format describer.
             */
            virtual void error (const char* format, ...) = 0;

            /**
             * \brief setup logger
             * \param configuration logger configuration object.
             */
            virtual void setup (const LoggerConfiguration &configuration) = 0;

        protected :
            /// \brief default constructor
            ILogger(): isSet (false) {}

            /// \brief constructor
            explicit ILogger (bool _isSet): isSet (_isSet) {}

            /// \brief Flag set to true to enable logs, false otherwise.
            bool isSet;
    };
}

#endif
