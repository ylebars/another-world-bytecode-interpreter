#ifndef __SFXPLAYER_HPP__
#define __SFXPLAYER_HPP__

/**
 * \file sfxplayer.hpp
 * \brief Definition for game sound effects.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \author Glaize, Sylvain
 *
 * It defines some instrument, that is some samples defining the core of the
 * instrument, these samples being read at different rate to shift their pitch.
 */

#include <cstdint>
#include <mutex>
#include <cstddef>

namespace AnotherWorld {
    // Forward declarations.
    class SDLStub;
    class Mixer;
    class Resource;

    /// \brief Maximum number of samples in a module.
    const std::size_t maxSample = 15;

    /// \brief An instrument is an abstraction to play a sound.
    struct SfxInstrument {
        /// \brief Pointer to sound data in memory.
        std::uint8_t* data;

        /// \brief Requested sound volume.
        std::uint8_t volume;
    };

    /// \brief Gather several samples.
    struct SfxModule {
        /// \brief Pointer to data in memory.
        const std::uint8_t* data;

        /// \brief Position of data currently being read.
        std::uint16_t curPos;

        /// \brief To be completed ...
        std::uint8_t curOrder;

        /// \brief To be completed ...
        std::uint8_t numOrder;

        /// \brief To be completed ...
        std::uint8_t orderTable [0x80];

        /// \brief Set of samples for the module.
        SfxInstrument instruments [maxSample];
    };

    class SfxPlayer {
        public:
            /**
             * \brief Constructor.
             * \param _mixer Mixer describer.
             * \param _res Localisation of resources in memory.
             * \param _sys System describer.
             */
            SfxPlayer (Mixer* _mixer, Resource* _res, SDLStub* _sys):
                    mixer (_mixer), res (_res), sys (_sys),
                    activeTimeId (0), delay (0), currentResource (0), sfxMod (),
                    markVar (nullptr) {}

            /**
             * \brief Copy constructor.
             * \param other The object to be copied.
             */
            SfxPlayer (const SfxPlayer &other): mixer (other.mixer),
                                                res (other.res),
                                                sys (other.sys),
                                              activeTimeId (other.activeTimeId),
                                                delay (other.delay),
                                        currentResource (other.currentResource),
                                                sfxMod (other.sfxMod),
                                                markVar (other.markVar) {}

            /**
             * \brief Move constructor.
             * \param other The object to be copied.
             */
            SfxPlayer (SfxPlayer &&other): mixer (other.mixer),
                                           res (other.res), sys (other.sys),
                                           activeTimeId (other.activeTimeId),
                                           delay (other.delay),
                                        currentResource (other.currentResource),
                                           sfxMod (other.sfxMod),
                                           markVar (other.markVar) {
                other.mixer = nullptr;
                other.res = nullptr;
                other.sys = nullptr;
                other.markVar = nullptr;
            }

            /// \brief Destructor.
            ~SfxPlayer () {stop();}

            /**
             * \brief Copy assignment operator.
             * \param other Object to be copied.
             */
            SfxPlayer &operator = (const SfxPlayer &other) {
                if (&other != this) {
                    mixer = other.mixer;
                    res = other.res;
                    sys = other.sys;
                    activeTimeId = other.activeTimeId;
                    delay = other.delay;
                    currentResource = other.currentResource;
                    sfxMod = other.sfxMod;
                    markVar = other.markVar;
                }

                return *this;
            }

            /**
             * \brief Move assignment operator.
             * \param other Object to be copied.
             */
            SfxPlayer &operator = (SfxPlayer &&other) {
                if (&other != this) {
                    mixer = other.mixer;
                    res = other.res;
                    sys = other.sys;
                    activeTimeId = other.activeTimeId;
                    delay = other.delay;
                    currentResource = other.currentResource;
                    sfxMod = other.sfxMod;
                    markVar = other.markVar;

                    other.mixer = nullptr;
                    other.res = nullptr;
                    other.sys = nullptr;
                    other.markVar = nullptr;
                }

                return *this;
            }

            /**
             * \brief Set markVar value.
             * \param _markVar Value.
             */
            void setMarkVar (std::int16_t* _markVar) {markVar = _markVar;}

            /**
             * \brief To be completed ...
             * \param delay Requested delay for the effect.
             */
            void setEventsDelay (std::uint16_t delay);

            /**
             * \brief Load a sound effect module from resources.
             * \param resourceIndex Resource describer.
             * \param _delay To be completed ...
             * \param pos To be completed ...
             */
            void loadSfxModule (std::uint16_t resourceIndex, std::uint16_t _delay,
                                std::uint8_t pos);

            /**
             * \brief Setting up the module.
             * \param data To be completed.
             */
            void prepareInstruments (const std::uint8_t* data);

            /// \brief Start sound effect.
            void start ();

            /// \brief Stop sound effect.
            void stop ();

            /// \brief Interaction with game events.
            void handleEvents ();

            /**
             * \brief To be completed ...
             * \param channel Channel in which the sound effect is to be played.
             * \param patternData To be completed.
             */
            void handlePattern (std::uint8_t channel,
                                const std::uint8_t* patternData);

            /**
             * \brief To be completed ...
             * \param interval To be completed ...
             * \param param To be completed ...
             */
            static std::uint32_t eventsCallback (std::uint32_t interval,
                                                 void* param);

        private:
            /// \brief Sound mixer for playing the effect.
            Mixer* mixer;

            /// \brief Pointer to resources in memory.
            Resource* res;

            /// \brief System descriptor.
            SDLStub* sys;

            /// \brief Mutex for synchronisation.
            std::mutex mutex;

            /// \brief To be completed ...
            int activeTimeId;

            /// \brief To be completed ...
            std::uint16_t delay;

            /// \brief Resource identifier.
            std::uint16_t currentResource;

            /// \brief Sound effect module.
            SfxModule sfxMod;

            /// \brief To be completed ...
            std::int16_t* markVar;

            /**
             * \brief Uses a sample to play a note.
             * \param channel Audio channel in which play the note.
             * \param note Frequency of the note.
             * \param instrumentSampleData Sample defining the instrument.
             * \param adjustedVolume Actual volume to which play the note.
             */
            void playNote (std::uint8_t channel, std::uint16_t note,
                           const std::uint8_t* instrumentSampleData,
                           std::int32_t adjustedVolume);
    };
}

#endif
