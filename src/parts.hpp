#ifndef __PARTS_HPP__
#define __PARTS_HPP__

/**
 * \file parts.hpp
 * \brief Manage game parts.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \author Willll
 * \author Glaize, Sylvain
 */

#include <cstdint>

namespace AnotherWorld {
    /// \brief Game parts resource indices.
    namespace Game {
        /// \brief Enumeration for all game parts.
        enum PartIdentifier: std::uint16_t {
            INVALID_PART = 0x0000,
            PART_FIRST = 0x3E80,
            PART1 = 0x3E80,
            PART2 = 0x3E81,  // Introduction
            PART3 = 0x3E82,
            PART4 = 0x3E83,  // Wake up in the suspended jail
            PART5 = 0x3E84,
            PART6 = 0x3E85,  // BattleChar sequence
            PART7 = 0x3E86,
            PART8 = 0x3E87,
            PART9 = 0x3E88,
            PART10 = 0x3E89,
            PART_LAST = 0x3E89
        };
    }

    /// \brief Game parts retrieval.
    namespace Parts {
        /// \brief Structure to retrieve the game parts resource indices.
        struct PartResourceIndices {
            /// \brief Palette identifier.
            std::uint8_t paletteIndex;
            /// \brief Game part code.
            std::uint8_t codeIndex;
            /// \brief Cut scene identifier.
            std::uint8_t videoCinematicIndex;
            /// \brief To be completed.
            std::uint8_t video2Index;
        };

        /// \brief Identifier for unreferenced part.
        const std::uint8_t UNREFERENCED = 0;

        /**
         * \brief Retrieves the game parts resource indices.
         * \param partId The identifier for the Game Part.
         * \returns The indices of the needed resources for this game part.
         */
        PartResourceIndices getResourceIndices (Game::PartIdentifier partId);
    }
}

#endif
