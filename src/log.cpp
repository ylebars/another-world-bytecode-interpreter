/**
 * \file log.cpp
 * \brief Class to manage logs
 * \author Willll
 */

#include "log.hpp"
#include "logSDL.hpp"
#include <stdexcept>

#include "errors.hpp"

namespace AnotherWorld {
    using LoggingClass=AnotherWorld::LogSDL;

    /// \brief Logger static object
    ILogger *Logger::logger = nullptr;

    Logger::Logger() {
        // Nothing to do here
    }

    Logger::~Logger() {
        if (logger) {
            delete logger;
        }
    }

    ILogger &Logger::getLogger() {
        if (logger) {
            return *logger;
        } else {
            throw UninitialisedLogFile ("Impossible to initialise logging file.");
        }
    }

    void Logger::loggerFactory (const LoggerConfiguration &configuration) {
        if (!logger) {
            logger = new LoggingClass(configuration.isLogEnable());
            logger->setup(configuration);
        } else {
            throw AlreadyInitialisedLogFile ("Logging file already initialized");
        }
    }
}
