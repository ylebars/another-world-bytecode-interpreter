/**
 * \file titleDetector.cpp
 * \brief Implementation of game version detection.
 * \author Glaize, Sylvain
 * \author Le Bars, Yoann
 */

#include "titleDetector.hpp"

#include <boost/filesystem.hpp>
#include <tuple>
#include <functional>
#include <cstddef>

#include "file.hpp"
#include "log.hpp"

namespace AnotherWorld {
    /// \brief Name for unknown versions.
    const std::string GAME_UNKNOWN = "Unknown game version";

    /// \brief Name of European version.
    const std::string GAME_TITLE_EU = "Another World";

    /// \brief Name of the North American version.
    const std::string GAME_TITLE_US = "Out Of This World";

    /// \brief Name of the Japanese version.
    const std::string GAME_TITLE_JP = "Outer World";

    /// \brief Name of the 15th anniversary edition.
    const std::string GAME_TITLE_15TH_ED =
        "Another World 15th anniversary edition";

    /// \brief Name of the 20th anniversary edition.
    const std::string GAME_TITLE_20TH_ED =
        "Another World 20th anniversary edition";

    /**
     * \brief Check if a string ends with a given suffix.
     * \param stringToCheck String being tested.
     * \returns Whether or not the suffix is found.
     *
     * This function exists in C++20. This project targets C++17 .
     */
    bool ends_with(const std::string& stringToCheck,
                   const std::string& suffix) {
        auto whereIsSuffix = stringToCheck.rfind(suffix);
        return whereIsSuffix == stringToCheck.size() - suffix.size();
    }

    /**
     * \brief Translate data type into a string.
     * \param dataType Data type to translate into a string.
     * \returns Data type as a string.
     */
    std::string dataTypeToString (DataType dataType) {
        switch (dataType) {
            case DT_15TH_EDITION:
                return "15TH EDITION";
            case DT_20TH_EDITION:
                return "20TH EDITION";
            case DT_DOS:
                return "DOS";
            case DT_AMIGA:
                return "AMIGA";
            case DT_ATARI:
                return "ATARI";
            case DT_WIN31:
                return "WINDOWS 3.1";
            case DT_3DO:
                return "3DO";
            case DT_UNKNOWN:
            default:
                return "UNKNOWN";
        }
    }

    /// \brief Namespace for functions checking game version.
    namespace CheckerFunctions {
        namespace fs = boost::filesystem;

        /**
         * \brief Check whether the version is the 15th anniversary version.
         * \param directory Directory where to find game asset files.
         * \returns Whether the version is the 15th anniversary version.
         */
        bool check15th (const std::string &directory) {
            /// Path where game files are stored.
            fs::path dataPath = directory;
            dataPath /= "Data";
            dataPath /= "Pak01.pak";
            /// Path name with type case correction.
            const auto concreteDataPath = getActualFilename(dataPath);

            return fs::exists(concreteDataPath);
        }

        /**
         * \brief Check whether the version is the 20th anniversary version.
         * \param directory Directory where to find game asset files.
         * \returns Whether the version is the 20th anniversary version.
         */
        bool check20th (const std::string &directory) {
            /// Path where game files are stored.
            fs::path dataPath = directory;
            dataPath /= "game";
            dataPath /= "DAT";
            dataPath /= "FILE017.DAT";
            /// Path name with type case correction.
            const auto concreteDataPath = getActualFilename(dataPath);

            return fs::exists(concreteDataPath);
        }

        /**
         * \brief Check whether the version is the MS-DOS version.
         * \param directory Directory where to find game asset files.
         * \returns Whether the version is the MS-DOS version.
         */
        bool checkDOS (const std::string &directory) {
            /// Path where game files are stored.
            fs::path dataPath = directory;
            dataPath /= "memlist.bin";
            /// Path name with type case correction.
            const auto concreteDataPath = getActualFilename(dataPath);

            return fs::exists(concreteDataPath);
        }

        /**
         * \brief Check whether the version is the Amiga or Atari ST version.
         * \param directory Directory where to find game asset files.
         * \returns Whether the version is the Amiga version, Atari ST version,
         * or another version.
         */
        DataType checkAmigaAtari (const std::string &directory) {
            /// Size of Amiga French version game assets.
            static const std::size_t FILESIZE_AMIGA_FR = 244674;
            /// Size of Amiga English version game assets.
            static const std::size_t FILESIZE_AMIGA_EN = 244868;
            /// Size of Atari ST English version game assets.
            static const std::size_t FILESIZE_ATARI_EN = 227142;
            /// Size of Atari ST French version game assets.
            static const std::size_t FILESIZE_ATARI_FR = 227096;

            /// Path where game files are stored.
            const auto dataPath = fs::path{directory} / "BANK01";
            /// Path name with case correction.
            const auto concreteDataPath = getActualFilename(dataPath);

            try {
                auto fileSize = file_size(concreteDataPath);

                switch (fileSize) {
                    case FILESIZE_AMIGA_EN:
                    case FILESIZE_AMIGA_FR:
                        return DT_AMIGA;
                    case FILESIZE_ATARI_EN:
                    case FILESIZE_ATARI_FR:
                        return DT_ATARI;
                    default:
                        return DT_UNKNOWN;
                }
            } catch (fs::filesystem_error &) {
                return DT_UNKNOWN;
            }
        }

        /**
         * \brief Check whether the version is the Amiga version.
         * \param directory Directory where to find game asset files.
         * \returns Whether the version is the Amiga version.
         */
        bool checkAmiga (const std::string &directory) {
            return checkAmigaAtari(directory) == DT_AMIGA;
        }

        /**
         * \brief Check whether the version is the Atari ST version.
         * \param directory Directory where to find game asset files.
         * \returns Whether the version is the Atari ST version.
         */
        bool checkAtari (const std::string &directory) {
            return checkAmigaAtari(directory) == DT_ATARI;
        }

        /**
         * \brief Check whether the version is the Microsoft Windows 3.1 version.
         * \param directory Directory where to find game asset files.
         * \returns Whether the version is the Microsoft Windows 3.1 version.
         */
        bool checkWin31 (const std::string &directory) {
            /// Path where game files are stored.
            fs::path dataPath = directory;
            dataPath /= "BANK";
            /// Path name with type case correction.
            const auto concreteDataPath = getActualFilename(dataPath);

            return fs::exists(concreteDataPath);
        }

        /**
         * \brief Check whether the version is the 3DO version.
         * \param directory Directory where to find game asset files.
         * \returns Whether the version is the 3DO version.
         */
        bool check3DO (const std::string &directory) {
            if (directory.size() > 5 && ends_with(directory, ".iso")) {
                return fs::exists(directory);
            } else {
                /// Path where game files are stored.
                fs::path dataPath = directory;
                dataPath /= "GameData";
                dataPath /= "File340";
                /// Path name with type case correction.
                const auto concreteDataPath = getActualFilename(dataPath);

                return fs::exists(concreteDataPath);
            }
        }
    }
}

AnotherWorld::TitleDetection::TitleDetection (const std::string &directory) {
    namespace cf = CheckerFunctions;
    /// Enumerate checking functions.
    std::tuple<std::function<bool (const std::string &)>, DataType>
        checkers[] = {
            {cf::check15th, DT_15TH_EDITION},
            {cf::check20th, DT_20TH_EDITION},
            {cf::checkDOS, DT_DOS},
            {cf::checkAmiga, DT_AMIGA},
            {cf::checkAtari, DT_ATARI},
            {cf::checkWin31, DT_WIN31},
            {cf::check3DO, DT_3DO},
        };

    /// Which game version the files stand for.
    auto checkedType =
        std::find_if(begin(checkers), end(checkers),
                     [&directory](decltype(checkers[0]) checkerAndType) {
                         return std::get<0>(checkerAndType)(directory);
                     });

    if (checkedType != end(checkers)) {
        dataType = std::get<1>(*checkedType);
    }
}

std::string AnotherWorld::TitleDetection::getGameTitle
        (Language language) const {
    switch (dataType) {
        case DT_15TH_EDITION:
            return GAME_TITLE_15TH_ED;
        case DT_20TH_EDITION:
            return GAME_TITLE_20TH_ED;
        case DT_DOS:
            if (language == LANG_US) {
                return GAME_TITLE_US;
            } else {
                return GAME_TITLE_EU;
            }
        case DT_AMIGA:
        case DT_ATARI:
        case DT_WIN31:
        case DT_3DO:
            return GAME_TITLE_EU;
        case DT_UNKNOWN:
        default:
            return GAME_UNKNOWN;
    }
}

void AnotherWorld::logsTitleDetection (const TitleDetection &titleDetection) {
    Logger::getLogger().debug(ILogger::DBG_RES, "Resource data type detected: %s",
          AnotherWorld::dataTypeToString(titleDetection.getDataType()).c_str());
}
