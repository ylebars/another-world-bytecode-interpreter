/**
 * \file joystick.cpp
 * \brief Implementation for joysticks and gamepads handling.
 * \author willll
 */

#include "joystick.hpp"

#include <algorithm>
#include <SDL.h>

#include "../log.hpp"
#include "../sys.hpp"

void AnotherWorld::Control::Joystick::open (int device) {
    if(!SDL_WasInit(SDL_INIT_JOYSTICK) ||
       !SDL_WasInit(SDL_INIT_GAMECONTROLLER)) {
        Logger::getLogger().error(
                "Joystick::open() SDL game controller not initialized");
    }

    /* Only one joystick/gamepad connected at a time. */
    if (_isConnected) {
        close();
    }

    if (SDL_IsGameController(device)) {
        _gamepad = SDL_GameControllerOpen(device);
        if (!_gamepad) {
            Logger::getLogger().warning(
                    "Joystick::open() Couldn't open game controller %d even though it has been detected",
                    device);
        } else {
            _joystick = SDL_GameControllerGetJoystick(_gamepad);    // At this point it should work
            _type = GAMEPAD;
        }
    } else {
        _joystick = SDL_JoystickOpen(device);
        _gamepad = nullptr;

        if (!_joystick) {
            Logger::getLogger().warning(
                    "Joystick::open() Couldn't open joystick  %d even though it has been detected",
                    device);
        } else {
            _type = JOYSTICK;
        }
    }

    if (_joystick) {
        _instanceID = SDL_JoystickInstanceID(_joystick);
        _isConnected = true;

        Logger::getLogger().information("Joystick::open() Opened game controller %d",
                                        device);
        Logger::getLogger().information("Joystick::open() Name: %s",
                                        SDL_JoystickNameForIndex(device));
        Logger::getLogger().information("Joystick::open() Number of Axes: %d",
                                        SDL_JoystickNumAxes(_joystick));
        Logger::getLogger().information(
                "Joystick::open() Number of Buttons: %d",
                SDL_JoystickNumButtons(_joystick));
    } else {
        Logger::getLogger().warning(
                "Joystick::open() Couldn't open game controller :  %d", device);
    }
}

void AnotherWorld::Control::Joystick::close () {
    if (_instanceID != -1) {
        Logger::getLogger().information(
                "Joystick::close() Closing joystick : %d", _instanceID);
    }
    if (_gamepad && SDL_GameControllerGetAttached(_gamepad)) {
        SDL_GameControllerClose(_gamepad);
    } else if (_joystick && SDL_JoystickGetAttached(_joystick)) {
        SDL_JoystickClose(_joystick);
    }
    _gamepad = nullptr;
    _joystick = nullptr;
    _instanceID = -1;
    _isConnected = false;
    _type = NONE;
}

bool AnotherWorld::Control::Joystick::processEvents (const SDL_Event& event,
                                                     SDLStub& sys) {
    bool pressed = false;
    switch (event.type) {
        case SDL_CONTROLLERDEVICEADDED:
            if (_isConnected) {
                auto item = std::make_pair(GAMEPAD, event.cdevice.which);
                auto it = std::find(_nextController.begin(),
                                    _nextController.end(), item);
                if (event.cdevice.which != _instanceID &&
                    _type != GAMEPAD &&
                    it == _nextController.end()) {
                    _nextController.push_front(item);
                    Logger::getLogger().information(
                            "Joystick::processEvents() New gaming device connected but ignored for now");
                } else {
                    Logger::getLogger().information(
                            "Joystick::processEvents() Gaming device already detected : %d",
                            event.cdevice.which);
                }
            } else {
                open(event.cdevice.which);
            }
            pressed = true;
            break;
        case SDL_JOYDEVICEADDED:
            if (_isConnected) {
                auto item = std::make_pair(JOYSTICK, event.jdevice.which);
                auto it = std::find(_nextController.begin(),
                                    _nextController.end(), item);
                if (event.jdevice.which != _instanceID &&
                    _type != JOYSTICK &&
                    it == _nextController.end()) {
                    _nextController.push_front(item);
                    Logger::getLogger().information(
                            "Joystick::processEvents() New joystick connected but ignored for now");
                } else {
                    Logger::getLogger().information(
                            "Joystick::processEvents() Joystick already detected : %d",
                            event.jdevice.which);
                }
            } else {
                open(event.jdevice.which);
            }
            pressed = true;
            break;
        case SDL_CONTROLLERDEVICEREMOVED:
            if (event.cdevice.which == _instanceID) {
                close();
                if (!_nextController.empty()) {
                    open(0);
                    _nextController.pop_front();
                }
            } else {
                auto item = std::make_pair(GAMEPAD, event.cdevice.which);
                auto it = std::find(_nextController.begin(),
                                    _nextController.end(), item);
                if (it != _nextController.end()) {
                    _nextController.erase(it);
                }
            }
            pressed = true;
            break;
        case SDL_JOYDEVICEREMOVED:
            if (event.jdevice.which == _instanceID) {
                close();
                if (!_nextController.empty()) {
                    open(0);
                    _nextController.pop_front();
                }
            } else {
                auto item = std::make_pair(JOYSTICK, event.jdevice.which);
                auto it = std::find(_nextController.begin(),
                                    _nextController.end(), item);
                if (it != _nextController.end()) {
                    _nextController.erase(it);
                }
            }
            pressed = true;
            break;
    }

    if (!pressed) {
        switch (_type) {
            case GAMEPAD:
                return processGamepadEvents(event, sys);
            case JOYSTICK :
                return processJoystickEvents(event, sys);
            default :
                break;
        }
    }

    return pressed;
}

bool AnotherWorld::Control::Joystick::processGamepadEvents (const SDL_Event& event,
                                                     SDLStub& sys) {
    /// Analog joystick dead zone
    const int JOYSTICK_DEAD_ZONE = 8000;
    bool pressed = false;
    switch (event.type) {
        case SDL_JOYAXISMOTION:
        case SDL_CONTROLLERAXISMOTION:

            Logger::getLogger().information("Controller axis %s changed to %d\n", SDL_GameControllerGetStringForAxis((SDL_GameControllerAxis)event.caxis.axis), event.caxis.value);

            if (event.caxis.which == _instanceID) {
                /// X axis motion
                if (event.caxis.axis == 0) {
                    /// Left of dead zone
                    if (event.caxis.value < -JOYSTICK_DEAD_ZONE) {
                        sys.input.dirMask |= PlayerInput::DIR_LEFT;
                        //Right of dead zone
                    } else if (event.caxis.value > JOYSTICK_DEAD_ZONE) {
                        sys.input.dirMask |= PlayerInput::DIR_RIGHT;
                    } else {
                        sys.input.dirMask &= ~(PlayerInput::DIR_RIGHT
                                               | PlayerInput::DIR_LEFT);
                    }
                /// Y axis motion
                } else if (event.caxis.axis == 1) {
                    /// Below of dead zone
                    if (event.caxis.value < -JOYSTICK_DEAD_ZONE) {
                        sys.input.dirMask |= PlayerInput::DIR_UP;
                        /// Above of dead zone
                    } else if (event.caxis.value > JOYSTICK_DEAD_ZONE) {
                        sys.input.dirMask |= PlayerInput::DIR_DOWN;
                    } else {
                        sys.input.dirMask &= ~(PlayerInput::DIR_UP
                                               | PlayerInput::DIR_DOWN);
                    }
                }
            } else {
                sys.input.dirMask &= ~(PlayerInput::DIR_UP
                                       | PlayerInput::DIR_DOWN
                                       | PlayerInput::DIR_RIGHT
                                       | PlayerInput::DIR_LEFT);
            }
            pressed = true;
            break;
        case SDL_CONTROLLERBUTTONDOWN:
            Logger::getLogger().information("Controller button %s %s\n", SDL_GameControllerGetStringForButton((SDL_GameControllerButton)event.cbutton.button), event.cbutton.state ? "pressed" : "released");
                switch (event.cbutton.button) {
                    case SDL_CONTROLLER_BUTTON_A:
                    case SDL_CONTROLLER_BUTTON_B:
                    case SDL_CONTROLLER_BUTTON_BACK:
                    case SDL_CONTROLLER_BUTTON_START:
                    case SDL_CONTROLLER_BUTTON_LEFTSHOULDER:
                    case SDL_CONTROLLER_BUTTON_RIGHTSHOULDER:
                        sys.input.button = true;
                        pressed = true;
                        break;
                    case SDL_CONTROLLER_BUTTON_DPAD_UP:
                        sys.input.dirMask |= PlayerInput::DIR_UP;
                        pressed = true;
                        break;
                    case SDL_CONTROLLER_BUTTON_DPAD_DOWN:
                        sys.input.dirMask |= PlayerInput::DIR_DOWN;
                        pressed = true;
                        break;
                    case SDL_CONTROLLER_BUTTON_DPAD_LEFT:
                        sys.input.dirMask |= PlayerInput::DIR_LEFT;
                        pressed = true;
                        break;
                    case SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
                        sys.input.dirMask |= PlayerInput::DIR_RIGHT;
                        pressed = true;
                        break;
                }
            break;
        case SDL_CONTROLLERBUTTONUP:
            Logger::getLogger().information("Controller button %s %s\n", SDL_GameControllerGetStringForButton((SDL_GameControllerButton)event.cbutton.button), event.cbutton.state ? "pressed" : "released");
            switch (event.cbutton.button) {
                case SDL_CONTROLLER_BUTTON_A:
                case SDL_CONTROLLER_BUTTON_B:
                case SDL_CONTROLLER_BUTTON_BACK:
                case SDL_CONTROLLER_BUTTON_START:
                case SDL_CONTROLLER_BUTTON_LEFTSHOULDER:
                case SDL_CONTROLLER_BUTTON_RIGHTSHOULDER:
                    sys.input.button = false;
                    pressed = true;
                    break;
                case SDL_CONTROLLER_BUTTON_DPAD_UP:
                    sys.input.dirMask &= ~PlayerInput::DIR_UP;
                    pressed = true;
                    break;
                case SDL_CONTROLLER_BUTTON_DPAD_DOWN:
                    sys.input.dirMask &= ~PlayerInput::DIR_DOWN;
                    pressed = true;
                    break;
                case SDL_CONTROLLER_BUTTON_DPAD_LEFT:
                    sys.input.dirMask &= ~PlayerInput::DIR_LEFT;
                    pressed = true;
                    break;
                case SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
                    sys.input.dirMask &= ~PlayerInput::DIR_RIGHT;
                    pressed = true;
                    break;
            }
            break;

        default:
            break;
    }

    return pressed;
}

bool AnotherWorld::Control::Joystick::processJoystickEvents (const SDL_Event& event,
                                                            SDLStub& sys) {
    /// Analog joystick dead zone
    const int JOYSTICK_DEAD_ZONE = 8000;
    bool pressed = false;
    switch (event.type) {

        case SDL_JOYAXISMOTION:
        case SDL_CONTROLLERAXISMOTION:

            Logger::getLogger().information("Controller axis %s changed to %d\n", SDL_GameControllerGetStringForAxis((SDL_GameControllerAxis)event.caxis.axis), event.caxis.value);

            if (event.jaxis.which == _instanceID) {
                //X axis motion
                if (event.jaxis.axis == 0) {
                    /// Left of dead zone
                    if (event.jaxis.value < -JOYSTICK_DEAD_ZONE) {
                        sys.input.dirMask |= PlayerInput::DIR_LEFT;
                        //Right of dead zone
                    } else if (event.jaxis.value > JOYSTICK_DEAD_ZONE) {
                        sys.input.dirMask |= PlayerInput::DIR_RIGHT;
                    } else {
                        sys.input.dirMask &= ~(PlayerInput::DIR_RIGHT
                                               | PlayerInput::DIR_LEFT);
                    }
                /// Y axis motion
                } else if (event.jaxis.axis == 1) {
                    /// Below of dead zone
                    if (event.jaxis.value < -JOYSTICK_DEAD_ZONE) {
                        sys.input.dirMask |= PlayerInput::DIR_UP;
                        /// Above of dead zone
                    } else if (event.jaxis.value > JOYSTICK_DEAD_ZONE) {
                        sys.input.dirMask |= PlayerInput::DIR_DOWN;
                    } else {
                        sys.input.dirMask &= ~(PlayerInput::DIR_UP
                                               | PlayerInput::DIR_DOWN);
                    }
                }
            } else {
                sys.input.dirMask &= ~(PlayerInput::DIR_UP
                                       | PlayerInput::DIR_DOWN
                                       | PlayerInput::DIR_RIGHT
                                       | PlayerInput::DIR_LEFT);
            }
            pressed = true;
            break;
        case SDL_JOYBALLMOTION:
            Logger::getLogger().warning(
                    "Joystick::processJoystickEvents() SDL_JOYBALLMOTION detected, not code behind :(");
            pressed = true;
            break;
        case SDL_JOYHATMOTION :
            switch (SDL_JoystickGetHat(_joystick, event.jhat.hat)) {
                case SDL_HAT_LEFTUP:
                    sys.input.dirMask |= PlayerInput::DIR_LEFT
                                         | PlayerInput::DIR_UP;
                    pressed = true;
                    break;
                case SDL_HAT_LEFT:
                    sys.input.dirMask |= PlayerInput::DIR_LEFT;
                    pressed = true;
                    break;
                case SDL_HAT_LEFTDOWN:
                    sys.input.dirMask |= PlayerInput::DIR_LEFT
                                         | PlayerInput::DIR_DOWN;
                    pressed = true;
                    break;
                case SDL_HAT_UP:
                    sys.input.dirMask |= PlayerInput::DIR_UP;
                    pressed = true;
                    break;
                case SDL_HAT_DOWN:
                    sys.input.dirMask |= PlayerInput::DIR_DOWN;
                    pressed = true;
                    break;
                case SDL_HAT_RIGHTUP:
                    sys.input.dirMask |= PlayerInput::DIR_RIGHT
                                         | PlayerInput::DIR_UP;
                    pressed = true;
                    break;
                case SDL_HAT_RIGHT:
                    sys.input.dirMask |= PlayerInput::DIR_RIGHT;
                    pressed = true;
                    break;
                case SDL_HAT_RIGHTDOWN:
                    sys.input.dirMask |= PlayerInput::DIR_RIGHT
                                         | PlayerInput::DIR_DOWN;
                    pressed = true;
                    break;
                case SDL_HAT_CENTERED:
                    sys.input.dirMask = 0x0;
                    pressed = true;
                    break;
                default:
                    Logger::getLogger().warning(
                            "Joystick::processJoystickEvents() SDL_JOYHATMOTION detected, but event no recognized :(");
            }
            break;
        case SDL_JOYBUTTONDOWN:
            Logger::getLogger().information("Controller button %s %s\n", SDL_GameControllerGetStringForButton((SDL_GameControllerButton)event.cbutton.button), event.cbutton.state ? "pressed" : "released");
                switch (event.cbutton.button) {
                    case SDL_CONTROLLER_BUTTON_A:
                    case SDL_CONTROLLER_BUTTON_B:
                    case SDL_CONTROLLER_BUTTON_X:
                    case SDL_CONTROLLER_BUTTON_Y:
                    case SDL_CONTROLLER_BUTTON_BACK:
                    case SDL_CONTROLLER_BUTTON_START:
                    case SDL_CONTROLLER_BUTTON_LEFTSHOULDER:
                    case SDL_CONTROLLER_BUTTON_RIGHTSHOULDER:
                        sys.input.button = true;
                        pressed = true;
                        break;
                    case SDL_CONTROLLER_BUTTON_DPAD_UP:
                        sys.input.dirMask |= PlayerInput::DIR_UP;
                        pressed = true;
                        break;
                    case SDL_CONTROLLER_BUTTON_DPAD_DOWN:
                        sys.input.dirMask |= PlayerInput::DIR_DOWN;
                        pressed = true;
                        break;
                    case SDL_CONTROLLER_BUTTON_DPAD_LEFT:
                        sys.input.dirMask |= PlayerInput::DIR_LEFT;
                        pressed = true;
                        break;
                    case SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
                        sys.input.dirMask |= PlayerInput::DIR_RIGHT;
                        pressed = true;
                        break;
                }
            break;
        case SDL_JOYBUTTONUP:
            Logger::getLogger().information("Controller button %s %s\n", SDL_GameControllerGetStringForButton((SDL_GameControllerButton)event.cbutton.button), event.cbutton.state ? "pressed" : "released");
            switch (event.cbutton.button) {
                case SDL_CONTROLLER_BUTTON_A:
                case SDL_CONTROLLER_BUTTON_B:
                case SDL_CONTROLLER_BUTTON_X:
                case SDL_CONTROLLER_BUTTON_Y:
                case SDL_CONTROLLER_BUTTON_BACK:
                case SDL_CONTROLLER_BUTTON_START:
                case SDL_CONTROLLER_BUTTON_LEFTSHOULDER:
                case SDL_CONTROLLER_BUTTON_RIGHTSHOULDER:
                    sys.input.button = false;
                    pressed = true;
                    break;
                case SDL_CONTROLLER_BUTTON_DPAD_UP:
                    sys.input.dirMask &= ~PlayerInput::DIR_UP;
                    pressed = true;
                    break;
                case SDL_CONTROLLER_BUTTON_DPAD_DOWN:
                    sys.input.dirMask &= ~PlayerInput::DIR_DOWN;
                    pressed = true;
                    break;
                case SDL_CONTROLLER_BUTTON_DPAD_LEFT:
                    sys.input.dirMask &= ~PlayerInput::DIR_LEFT;
                    pressed = true;
                    break;
                case SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
                    sys.input.dirMask &= ~PlayerInput::DIR_RIGHT;
                    pressed = true;
                    break;
            }
            break;

        default:
            break;
    }

    return pressed;
}
