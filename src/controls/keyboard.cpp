/**
 * \file keyboard.cpp
 * \brief It is only possible to live happily ever after on a day-to-day basis.
 * \author willll
 */

#include "keyboard.hpp"

#include <SDL.h>
#include <cstdint>

#include "../sys.hpp"

bool AnotherWorld::Control::Keyboard::processEvents(const SDL_Event &event,
                                                    SDLStub &sys) {
    /// Describe keyboard state.
    const std::uint8_t *keystate = SDL_GetKeyboardState(nullptr);
    switch (event.type) {
        case SDL_QUIT:
            sys.input.quit = true;
            return true;
        case SDL_KEYUP:
            switch (event.key.keysym.sym) {
                case SDLK_LEFT:
                    sys.input.dirMask &= ~PlayerInput::DIR_LEFT;
                    return true;
                case SDLK_RIGHT:
                    sys.input.dirMask &= ~PlayerInput::DIR_RIGHT;
                    return true;
                case SDLK_UP:
                    sys.input.dirMask &= ~PlayerInput::DIR_UP;
                    return true;
                case SDLK_DOWN:
                    sys.input.dirMask &= ~PlayerInput::DIR_DOWN;
                    return true;
                case SDLK_SPACE:
                case SDLK_RETURN :
                    sys.input.button = false;
                    return true;
                case SDLK_q:
                case SDLK_ESCAPE:
                    sys.input.quit = true;
                    return true;
                case SDLK_s:
                    sys.input.mute = !sys.input.mute;
                    return true;
                case SDLK_PLUS:
                case SDLK_KP_PLUS:
                    if (keystate[SDL_SCANCODE_LALT]) {
                        if (sys.scale < 4) {
                            ++sys.scale;
                            sys.switchGfxMode();
                        }
                    } else {
                        sys.input.volumeUP = true;
                    }
                    return true;
                case SDLK_MINUS:
                case SDLK_KP_MINUS :
                    if (keystate[SDL_SCANCODE_LALT]) {
                        if (sys.scale > 1) {
                            --sys.scale;
                            sys.switchGfxMode();
                        }
                    } else {
                        sys.input.volumeDOWN = true;
                    }
                    return true;
                case SDLK_LALT :
                    if (keystate[SDL_SCANCODE_KP_PLUS]) {
                        if (sys.scale < 4) {
                            ++sys.scale;
                            sys.switchGfxMode();
                        }
                    } else if (keystate[SDL_SCANCODE_KP_MINUS]) {
                        if (sys.scale > 1) {
                            --sys.scale;
                            sys.switchGfxMode();
                        }
                    }
                    return true;
                case SDLK_f:
                    sys.fullscreen = !sys.fullscreen;
                    sys.switchGfxMode();
                    return true;
            }
            break;
        case SDL_KEYDOWN:
            sys.input.lastChar = event.key.keysym.sym;
            switch (event.key.keysym.sym) {
                case SDLK_LEFT:
                    sys.input.dirMask |= PlayerInput::DIR_LEFT;
                    return true;
                case SDLK_RIGHT:
                    sys.input.dirMask |= PlayerInput::DIR_RIGHT;
                    return true;
                case SDLK_UP:
                    sys.input.dirMask |= PlayerInput::DIR_UP;
                    return true;
                case SDLK_DOWN:
                    sys.input.dirMask |= PlayerInput::DIR_DOWN;
                    return true;
                case SDLK_SPACE:
                case SDLK_RETURN:
                    sys.input.button = true;
                    return true;
                case SDLK_c:
                    sys.input.code = true;
                    return true;
                case SDLK_p:
                    sys.input.pause = true;
                    return true;
                default:
                    break;
            }
            break;
        default:
            break;
    }
    return false;
}
