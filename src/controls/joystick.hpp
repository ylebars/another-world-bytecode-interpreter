#ifndef __JOYSTICK_HPP__
#define __JOYSTICK_HPP__

/**
 * \file joystick.hpp
 * \brief Definitions for handling joysticks and gamepads.
 * \author willll
 * \todo PS3 DPAD controller to fix
 * \todo USB XBOX360 DPAD controller to fix
 * \todo ADD/REMOVE gamepads to fix
 */


#include "icontroller.hpp"

#include <deque>
#include <utility>

#include <SDL_joystick.h>
#include <SDL_gamecontroller.h>

namespace AnotherWorld {
    namespace Control {
        /// \brief Handler for joysticks and gamepads.
        class Joystick: public IController {
            public :
                /// \brief Default constructor.
                Joystick (): _joystick (nullptr), _gamepad (nullptr),
                             _instanceID (-1), _isConnected (false) {}

                /// \brief Destructor.
                ~Joystick () {close();}

                /**
                 * \brief Opens a controller.
                 * \param device Device identifier.
                 */
                void open (int device) override;

                /// \brief Close controller.
                void close () override;

                /**
                 * \brief Process controller events.
                 * \param event Event to be processed.
                 * \returns Whether or not it has been able to handle the event.
                 */
                bool processEvents (const SDL_Event &event,
                                    SDLStub &sys) override;

            protected :
                /// \brief Describer of the controller being handled.
                SDL_Joystick* _joystick = nullptr;

                /// \brief Describe the type of controller.
                SDL_GameController* _gamepad = nullptr;

                /// \brief ID of the game controller.
                SDL_JoystickID _instanceID = -1;

                /// \brief Whether or not the controller is connected.
                bool _isConnected = false;

                /// \brief Type of controller
                Type _type = NONE;

                /// \brief Controllers queue.
                std::deque<std::pair<Type, SDL_JoystickID>> _nextController;

                /**
                * \brief Process gamepad events.
                * \param event Event to be processed.
                * \returns Whether or not it has been able to handle the event.
                */
                bool processGamepadEvents (const SDL_Event &event,
                                    SDLStub &sys);

                /**
                * \brief Process joystick events.
                * \param event Event to be processed.
                * \returns Whether or not it has been able to handle the event.
                */
                bool processJoystickEvents (const SDL_Event &event,
                                           SDLStub &sys);
            };
    }
}

#endif // __JOYSTICK_HPP__
