/**
 * \file main.cpp
 * \brief A bytecode interpreter for Éric Chahi's classical game Another World.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \author Willll
 */

/**
 * \mainpage
 *
 * Raw is a reimplementation of the virtual machine for Another World. This
 * program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License  as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version. See file `LICENSE.txt` or go to:
 *
 * <https://choosealicense.com/licenses/gpl-2.0/>
 *
 * Command line:
 *
 *     raw [Options]
 *
 * Supported options:
 *
 *     -h [ --help ]              Display this help message.
 *     -v [ --version ]           Display program version.
 *     -d [ --datapath ] arg (=.) Path to game data.
 *     -m [ --mute ]              Mute the sound.
 *     -c [ --debugConsole ]      Enable logs through the standard output.
 *     -f [ --debugFile ] arg     Enable logs into a file.
 *     -s [ --debugSyslog ] arg   Enable logs through Syslog.
 *     -l [ --level ] arg         Log level filter.
 *
 * To obtain some help, simply type:
 *
 *     ./raw --help
 *
 * In order to run the game, you will need the original files. You can either:
 *
 * - put the data files of the game in the same directory than the executable,
 * the icon image (`logo-aw.bmp`), and the game controllers calibration file
 * (`gamecontrollerdb.txt`);
 * - use the `--datapath` command line option to specify the data files
 * directory (this is the recommended way).
 *
 * The icon image (`logo-aw.bmp`) as well as the game controllers calibration
 * file (`gamecontrollerdb.txt`) should always be located in the same directory
 * than the executable.
 *
 * The available log levels are:
 *
 * - debug
 * - information
 * - warning
 * - error
 *
 * The higher level also trigger on the lower levels. For instance, if you ask
 * for "warning" level, you will also get "error" level. Default is "error".
 *
 * Here are the various in game hotkeys:
 *
 * - \<Arrow Keys\>: allow you to move Lester
 * - \<Enter\>/\<Space\>: allow you run/shoot with your gun
 * - \<C\>: allow to enter a code to jump at a specific level
 * - \<P\>: pause the game
 * - \<Esc\>/\<Q\>: exit the game
 * - \<Alt\> + \<+\> and \<Alt\> + \<-\>: change window scale factor
 * - \<F\>: toggle full-screen mode
 * - \<S\>: toggle sound off and on
 * - \<+\> and \<-\>: increase and decrease sound level
 *
 *
 * \todo Add OpenGL support, using work from RawGL.
 */

#include <boost/program_options.hpp>
#include <iostream>
#include <string>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <exception>
#include <cstdint>

#include <config.hpp>

#include "errors.hpp"
#include "engine.hpp"
#include "sys.hpp"
#include "log.hpp"
#include "ilog.hpp"

/**
 * \brief Main function of the program.
 * \param argc Count of arguments transmitted to the program.
 * \param argv Values of arguments transmitted to the program.
 * \returns 0 if everything went well,
 *     -1 if the given data path does not exists,
 *     -2 if the given data path does not lead to a directory,
 *     -3 it the command line is incorrect,
 *     -4 if a standard exception occurred,
 *     -5 if an unknown exception occurred,
 *     -6 if a general error from the game source code occurred,
 *     -7 if game data file cannot be located,
 *     -8 if an error occurred while reading memory list,
 *     -9 when an error occurred while looking for game signature,
 *     -10 if no valid game signature has been found,
 *     -11 if cannot find a supported game asset format,
 *     -12 if an unrecoverable error occur when running the game,
 *     -13 if the logging file cannot be initialised,
 *     -14 if the logging file is already initialised,
 *     -15 if an invalid severity level is asked.
 */
int main(int argc, char** argv) {
    namespace po = boost::program_options;
    namespace fs = boost::filesystem;
    namespace algo = boost::algorithm;
    namespace aw = AnotherWorld;

    /// Default path where to find game data.
    const char* defaultDataPath = ".";
    /// Wanted sound level.
    const uint8_t defaultSoundVolume = 75;
    /// Default severity logging level.
    const aw::SeverityLevels defaultLogLevel = aw::SeverityLevels::DEBUG;
    /// Default name of the log file.
    const char* defaultFileLoggerFile = "debug.log";
    /// Default IP for logging system.
    const char* defaultsyslogLoggerIp = "localhost:514";
    /// Default port for logging system.
    const std::uint16_t defaultsyslogLoggerPort = 514;

    /* -- Reading the command line. -- */

    /// Path to game data.
    std::string dataPath = defaultDataPath;
    /// Mute Flag
    bool isMute  = false;
    /// Console Logger enable flag
    bool isConsoleLoggerEnable = false;
    /// File Logger file
    std::string fileLoggerfile = defaultFileLoggerFile;
    /// Syslog Logger IP
    std::string syslogLoggerIp = defaultsyslogLoggerIp;
    /// Logs level
    std::string logLevel = "";

    /// Logger object
    aw::Logger logger;

    try {
        /// Declaring supported options.
        po::options_description desc("Supported options");
        desc.add_options()
            ("help,h", "Display this help message.")
            ("version,v", "Display program version.")
            ("datapath,d",
             po::value<std::string>(&dataPath)->default_value(defaultDataPath),
             "Path to game data.")
            ("mute,m",
             po::bool_switch(&isMute),
             "Mute the sound.")
            ("debugConsole,c",
             po::bool_switch(&isConsoleLoggerEnable),
             "Enable logs through the standard output.")
            ("debugFile,f",
             po::value<std::string>(&fileLoggerfile),
             "Enable logs into a file.")
            ("debugSyslog,s", po::value<std::string>(&syslogLoggerIp),
             "Enable logs through Syslog.")
            ("level,l", po::value<std::string>(&logLevel),
             "Log level filter. The available log levels are:\n\n- debug\n"
             "- information\n- warning\n- error\n\nThe higher level also "
             "trigger on the lower levels. For instance, if you ask for "
             "“warning” level, you will also get “error” level. Default is "
             "“error”.");
        /// Command line.
        po::options_description cmd;
        cmd.add(desc);

        /// Options map.
        po::variables_map vm;
        po::store(po::command_line_parser(argc, argv).options(cmd).run(), vm);
        po::notify(vm);

        /// Indicates whether or not the program should be stopped.
        bool stop = false;

        /* Check if help message should be displayed. */
        if (vm.count("help")) {
            std::cout << "Raw is a reimplementation of the virtual machine "
                      << "for Another World.\n\n"
                      << "Command: \n\n"
                      << '\t' << argv[0] << " [Options]\n\n"
                      << desc << '\n';
            stop = true;
        }

        /* Check if program version should be displayed. */
        if (vm.count("version")) {
            std::cout << argv[0] << " version " << Configuration::versionMajor
                      << '.' << Configuration::versionMinor << '.'
                      << Configuration::patchVersion << '\n';
            stop = true;
        }

        if (stop) return 0;

        if (!fs::exists(dataPath)) {
            std::cerr << "Data path incorrect: \"" << dataPath
                      << "\" does not exist.\n";
            return -1;
        }
        if (!fs::is_directory(dataPath)) {
            std::cerr << "Data path incorrect: \"" << dataPath
                      << "\" does not lead to a directory.\n";
            return -2;
        }

        /* Format input log level */
        algo::trim(logLevel);
        algo::to_lower(logLevel);

        /* -- Logs Initialization -- */

        aw::LoggerConfiguration configuration (isConsoleLoggerEnable,
                                               (vm.count("debugFile") >= 1),
                                               (vm.count("debugSyslog") >= 1),
                                               !logLevel.empty()?
                                                    aw::to_SeverityLevel(logLevel):
                                                    defaultLogLevel,
                                               fileLoggerfile,
                                               [&](const std::string &ip){
                                                   /// Position of the port.
                                                   auto end = ip.find(':');
                                                   return (end != std::string::npos)?
                                                    ip.substr(0, end): ip;
                                               }(syslogLoggerIp),
                                               [&](const std::string &ip){
                                                   /// Position of the port.
                                                   auto end = ip.find(':');
                                                   // That's a hellish oneliner !
                                                   return (end != std::string::npos)?
                                                       static_cast<std::uint16_t>(std::stoi(ip.substr(end+1, ip.size()))):
                                                       defaultsyslogLoggerPort;
                                               }(syslogLoggerIp));

        logger.loggerFactory(configuration);

        /* -- Launch game. -- */

        /// System description.
        aw::SDLStub stub (argv[0]);

        /// Game engine instantiation.
        aw::Engine e (&stub, dataPath, isMute ? 0 : defaultSoundVolume);

        return e.run();
    }
    catch (po::error &e) {
        std::cerr << e.what() << '\n';
        return -3;
    }
    catch (aw::UnlocatableFile &e) {
        std::cerr << e.what() << '\n';
        return -7;
    }
    catch (aw::FailedSignatureReading &e) {
        std::cerr << e.what() << '\n';
        return -9;
    }
    catch (aw::NoValidSignature &e) {
        std::cerr << e.what() << '\n';
        return -10;
    }
    catch (aw::UnsupportedDataType &e) {
        std::cerr << e.what() << '\n';
        return -11;
    }
    catch (aw::UninitialisedLogFile &e) {
        std::cerr << e.what() << '\n';
        return -13;
    }
    catch (aw::AlreadyInitialisedLogFile &e) {
        std::cerr << e.what() << '\n';
        return -14;
    }
    catch (aw::InvalidSeverityLevel &e) {
        std::cerr << e.what() << '\n';
        return -15;
    }
    catch (aw::AWError &e) {
        std::cerr << e.what() << '\n';
        return -6;
    }
    catch (std::exception &e) {
        std::cerr << e.what() << '\n';
        return -4;
    }
    catch (...) {
        std::cerr << "An unknown exception occurred.\n";
        return -5;
    }
}
