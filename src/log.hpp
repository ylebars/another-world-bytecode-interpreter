#ifndef __LOG_HPP__
#define __LOG_HPP__

/**
 * \file log.hpp
 * \brief Class to manage logs
 * \author Willll
 */

#include <cstdarg>
#include <cstdint>

#include "ilog.hpp"

namespace AnotherWorld {
    /// \brief Class for execution logging.
    class Logger {
        public :
            /// \brief No copy constructor.
            Logger (Logger const &) = delete;
            /// \brief No move operator.
            Logger (Logger const &&) = delete;
            /// \brief No assignment operator.
            Logger& operator = (Logger const &) = delete;

            /// \brief Default constructor.
            Logger();

            /// \brief Default destructor.
            ~Logger();

            /**
             * \brief How to access the Logger, should be called to retrieve the
             * Logger object.
             * \returns The Logger object.
             * \throws UninitialisedLogFile
             */
            static ILogger &getLogger ();

            /**
             * \brief Logger factory method
             * \param configuration logger configuration object.
             * \throws AlreadyInitialisedLogFile
             */
            void loggerFactory (const LoggerConfiguration &configuration);

        private:
            /**
            * \brief Syslog sink configuration method
            * \param configuration logger configuration object.
            */
            static void setupSyslog (const LoggerConfiguration &configuration);

            /// Badly designed static ILogger object.
            static ILogger *logger;
    };
}
#endif
