/**
 * \file mixer.cpp
 * \brief Implementation for mixing audio channels.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \author Glaize, Sylvain
 */

#include "mixer.hpp"

#include <cstring>

#include "ilog.hpp"
#include "log.hpp"
#include "sys.hpp"

namespace AnotherWorld {
    /// \brief Name space for local utilitarian functions.
    namespace Private {
        /**
         * \brief Addition with boundary control.
         * \param a First value to add.
         * \param b Second value to add.
         * \returns The total.
         */
        std::int8_t addWithClamp (std::int32_t a, std::int32_t b) {
            /// Total without boundary control.
            int add = a + b;
            if (add < -128) {
                add = -128;
            } else if (add > 127) {
                add = 127;
            }
            return static_cast<std::int8_t>(add);
        }

        /**
         * \brief Interpolates the two designated samples by the
         * advancement factor.
         * \param data The buffer in which the samples are fetched.
         * \param firstSamplePosition The offset in the buffer for the first
         * sample.
         * \param secondSamplePosition The offset in the buffer for the second
         * sample.
         * \param interpolationFactor The interpolation factor. 0 is full first
         * sample, 0xff is full second sample.
         * \returns The interpolated sample.
         */
        std::int32_t interpolate (const std::uint8_t* data,
                                  std::uint16_t firstSamplePosition,
                                  std::uint16_t secondSamplePosition,
                                  std::uint16_t interpolationFactor) {
            /// Data of the first sample.
            const auto firstSample = *reinterpret_cast<const std::int8_t*>(
                data + firstSamplePosition);
            /// Data of the second sample.
            const auto secondSample = *reinterpret_cast<const std::int8_t*>(
                data + secondSamplePosition);

            /// Interpolation of the two sample.
            const auto interpolatedSum =
                (firstSample * (0xFF - interpolationFactor) +
                 secondSample * interpolationFactor);
            /// Normalized resulting sample.
            auto normalizedSample =
                static_cast<std::int8_t>(interpolatedSum >> 8);

            return normalizedSample;
        }

        /**
         * \brief Converts signed 8-bit PCM to unsigned 8-bit PCM. The
         * current(?) version of SDL hangs when using signed 8-bit
         * PCM in combination with the PulseAudio driver.
         * \param buffer The buffer to convert.
         * \param length The length of the buffer.
         */
        void bufferSignedToUnsigned (std::int8_t* buffer, int length) {
            /// Localisation in memory of the beginning of the sound data.
            const auto startOfBuffer = buffer;
            /// Localisation in memory of the end of the sound data.
            const auto endOfBuffer = buffer + length;

            for (int8_t* outBuffer = startOfBuffer; outBuffer < endOfBuffer;
                 ++outBuffer) {
                *reinterpret_cast<uint8_t*>(outBuffer) = (*outBuffer + 128);
            }
        }
    }

    Mixer::Mixer (SDLStub* stub, const uint8_t _volume): sys (stub), volumeFactor(_volume) {
        sys->startAudio(Mixer::mixCallback, this);
    }

    Mixer::~Mixer () {
        stopAll();
        sys->stopAudio();
    }

    void Mixer::playChannel (std::uint8_t channel, const MixerChunk &mixerChunk,
                             std::uint16_t freq, std::uint8_t volume) {
        Logger::getLogger().debug(ILogger::DBG_SND,
                                  "Mixer::playChannel(%d, %d, %d)", channel,
                                  freq, volume);

        /// The mutex is acquired in the constructor
        std::scoped_lock mutexLock{mutex};
        /// Reference to the mixer channels.
        MixerChannel &ch = channels.at(channel);
        ch.active = true;
        ch.requestedVolume = volume;
        ch.volume = volume * volumeFactor / 100;
        ch.chunk = mixerChunk;
        ch.positionInChunk = 0;
        ch.positionIncrement = (freq << 8) / sys->getOutputSampleRate();

    }

    void Mixer::stopChannel (std::uint8_t channel) {
        Logger::getLogger().debug(ILogger::DBG_SND, "Mixer::stopChannel(%d)",
                                  channel);

        /// Reference to current mutex.
        std::scoped_lock mutexLock {mutex};
        channels.at(channel).active = false;
    }

    void Mixer::setChannelVolume(std::uint8_t channel, std::uint8_t volume) {
        Logger::getLogger().debug(ILogger::DBG_SND,
                                  "Mixer::setChannelVolume(%d, %d)", channel,
                                  volume);

        /// Reference to current mutex.
        std::scoped_lock mutexLock {mutex};
        MixerChannel &ch = channels.at(channel);
        ch.requestedVolume = volume;
        ch.volume = volume * volumeFactor / 100;
    }

    void Mixer::stopAll () {
        Logger::getLogger().debug(ILogger::DBG_SND, "Mixer::stopAll()");
        /// Reference to current mutex.
        std::scoped_lock mutexLock{mutex};
        for (auto &channel : channels) {
            channel.active = false;
        }
    }

    void Mixer::mix (std::int8_t* buf, int len) {
        /// Reference to current mutex.
        std::scoped_lock mutexLock{mutex};

        /* Clear the buffer since nothing guaranty we are receiving clean
         * memory. */
        std::memset(buf, 0, len);

        if (!sys->input.mute) {
            for (auto &ch : channels) {
                if (!ch.active) {
                    continue;
                }

                /// Starting position of the buffer in memory.
                const auto startOfBuffer = buf;
                /// Ending position of the buffer in memory.
                const auto endOfBuffer = buf + len;

                for (std::int8_t* outBuffer = startOfBuffer;
                     outBuffer < endOfBuffer; ++outBuffer) {
                    /**
                     * The interpolation factor (between 0 and 255) between the
                     * first and second sample position.
                     */
                    std::uint16_t interpolationFactor =
                        (ch.positionInChunk & 0xFF);

                    /// Address of the first sample
                    std::uint16_t firstSamplePosition = ch.positionInChunk >> 8;

                    /// Advances the pointer.
                    ch.positionInChunk += ch.positionIncrement;

                    /// Address of the second sample
                    std::uint16_t secondSamplePosition;

                    if (ch.chunk.loopLen != 0) {
                        /* Computes the second sample in a looping chunk. */
                        if (firstSamplePosition ==
                            ch.chunk.loopPos + ch.chunk.loopLen - 1) {
                            Logger::getLogger().debug(
                                ILogger::DBG_SND,
                                "Looping sample on channel %d",
                                std::distance(&ch, channels.data()));
                            ch.positionInChunk = secondSamplePosition =
                                ch.chunk.loopPos;
                        } else {
                            secondSamplePosition = firstSamplePosition + 1;
                        }
                    } else {
                        /* Computes the second sample in a non looping chunk. */
                        if (firstSamplePosition == ch.chunk.len - 1) {
                            Logger::getLogger().debug(
                                ILogger::DBG_SND,
                                "Stopping sample on channel %d",
                                std::distance(&ch, channels.data()));
                            ch.active = false;
                            break;
                        } else {
                            secondSamplePosition = firstSamplePosition + 1;
                        }
                    }

                    /// Corrected sample.
                    auto interpolatedSample = Private::interpolate(
                        ch.chunk.data, firstSamplePosition,
                        secondSamplePosition, interpolationFactor);

                    /// Sets volume and clamp sample value.
                    *outBuffer = Private::addWithClamp(
                        *outBuffer,
                        interpolatedSample * ch.volume / 0x40);  // 0x40=64
                }
            }
        }

        Private::bufferSignedToUnsigned(buf, len);
        updateUserVolume();
    }

    void Mixer::updateUserVolume () {
        if (sys->input.volumeUP) {
            volumeFactor = std::min(volumeFactor + 10, 100);
            Logger::getLogger().log(INFORMATION, "Sound volume raised to : %d",
                                    volumeFactor);
        } else if (sys->input.volumeDOWN) {
            volumeFactor = std::max(volumeFactor - 10, 0);
            Logger::getLogger().log(INFORMATION, "Sound volume lowered to : %d",
                                    volumeFactor);
        }

        if (sys->input.volumeDOWN || sys->input.volumeUP) {
            for (auto& channel : channels) {
                channel.volume = channel.requestedVolume * volumeFactor / 100;
            }
            sys->input.volumeDOWN = false;
            sys->input.volumeUP = false;
        }
    }
}
