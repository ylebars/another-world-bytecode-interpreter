#ifndef __VIDEO_HPP__
#define __VIDEO_HPP__

/**
 * \file video.hpp
 * \brief Definition for graphic drawing in the game.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 */

#include "intern.hpp"

#include <cstdint>

namespace AnotherWorld {
    // Forward declarations.
    class SDLStub;
    class Resource;


    /// \brief Text of the game.
    struct StrEntry {
        /// \brief Text identifier.
        std::uint16_t id;

        /// \brief Actual text.
        const char *str;
    };

    /// \brief Polygons of the game.
    struct Polygon {
        /// \brief Maximum number of vertices in a polygon.
        enum {MAX_POINTS = 50};

        /// \brief To be completed ...
        std::uint16_t bbw;
        /// \brief To be completed ...
        std::uint16_t bbh;

        /// \brief Number of vertices in the polygon.
        std::uint8_t numPoints;

        /// \brief List of polygon vertices.
        AnotherWorld::Point points [MAX_POINTS];

        /**
         * \brief Read polygon vertices from data.
         * \param p Pointer to data in memory.
         * \param zoom How much the polygon is zoomed on screen.
         */
        void readVertices (const std::uint8_t* p, std::uint16_t zoom);
    };

    /**
     * \brief This is used to detect the end of _stringsTableEng and
     * _stringsTableDemo
     */
    const std::uint16_t END_OF_STRING_DICTIONARY = 0xFFFF;

    /// \brief Special value when no palette change is necessary.
    const std::uint8_t NO_PALETTE_CHANGE_REQUESTED = 0xFF;

    /// \brief Handling display.
    class Video {
        public:
            /**
             * \brief Constructor.
             * \param _res Pointer to resources.
             * \param stub System describer.
             */
            Video (Resource* _res, SDLStub* stub);

            /**
             * \brief Copy constructor.
             * \param other The object to be copied.
             */
            Video (const Video &other);

            /**
             * \brief Move constructor.
             * \param other The object to be moved.
             */
            Video (Video &&other);

            /// \brief Destructor.
            ~Video ();

            /**
             * \brief Copy assignment operator.
             * \param other Object to be copied.
             */
            Video &operator = (const Video &other);

            /**
             * \brief Move assignment operator.
             * \param other Object to be copied.
             */
            Video &operator = (Video &&other);

            /**
             * \brief Setter for paletteIdRequested.
             * \param _paletteIdRequested New value.
             */
            void setPaletteIdRequested (std::uint8_t _paletteIdRequested) {
                paletteIdRequested = _paletteIdRequested;
            }

            /**
             * \brief Gets data in buffer.
             * \param _dataBuf Data buffer.
             * \param offset Data offset in memory.
             */
            void setDataBuffer (std::uint8_t* _dataBuf, std::uint16_t offset) {
                dataBuf = _dataBuf;
                pData.pc = _dataBuf + offset;
            }

            /**
             * \brief Gets a polygon from memory and draws it.
             * \param color Polygon colour.
             * \param zoom How much the polygon is zoomed.
             * \param pt Polygon location on the screen.
             *
             * A shape can be given in two different ways:
             *    - A list of screenspace vertices.
             *    - A list of objectspace vertices, based on a delta from the
             *    first vertex.  
             *
             * This is a recursive function.
             */
            void readAndDrawPolygon (std::uint8_t color, std::uint16_t zoom,
                                     const Point &pt);

            /**
             * \brief Fills in a polygon.
             * \param color Filling colour.
             * \param zoom How much the polygon is zoomed.
             * \param pt Polygon location on the screen.
             */
            void fillPolygon (std::uint8_t color, std::uint16_t zoom,
                              const Point &pt);

            /**
             * \brief Gets a set of polygons from memory and draw them.
             * \param zoom How much the polygons are zoomed.
             * \param pt Set location on the screen.
             *
             * What is read from the bytecode is not a pure screenspace
             * polygon but polygonspace polygon.
             */
            void readAndDrawPolygonHierarchy (std::uint16_t zoom,
                                              const Point &pt);

            /**
             * \brief Calculates intermediate polygons configuration.
             * \param p1 Starting location.
             * \param p2 Ending location.
             * \param dy Displacement.
             * \returns New location.
             */
            std::int32_t calcStep (const Point &p1, const Point &p2,
                                   std::uint16_t &dy);

            /**
             * \brief Draws some text.
             * \param color Text colour.
             * \param x Text abscissa.
             * \param y Text ordinate.
             * \param strId Text identifier.
             */
            void drawString (std::uint8_t color, std::uint16_t x,
                             std::uint16_t y, std::uint16_t strId);

            /**
             * \brief Gets a pointer to a given video buffer.
             * \param page Video buffer identifier.
             * \returns Memory location of the asked video buffer.
             */
            std::uint8_t* getPage (std::uint8_t page);

            /**
             * \brief Swaps working video buffer.
             * \param page New working video buffer identifier.
             */
            void changePagePtr1 (std::uint8_t page);

            /**
             * \brief Entirely fills a video buffer with a given colour.
             * \param page Buffer to be fill.
             * \param color Filling colour.
             */
            void fillPage (std::uint8_t page, std::uint8_t color);

            /**
             * \brief Copies a video buffer into another.
             * \param src Source video buffer identifier.
             * \param dst Destination video buffer identifier.
             * \param vscroll Vertical scrolling value.
             *
             * This opcode is used once the background of a scene has been
             * drawn in one of the framebuffer: it is copied in the current
             * framebuffer at the start of a new frame in order to improve
             * performances.
             */
            void copyPage (std::uint8_t src, std::uint8_t dst,
                           std::int16_t vscroll);

            /**
             * \brief Copies a video buffer into the displaying video buffer.
             * \param src Pointer to source video buffer.
             */
            void copyPage (const std::uint8_t* src);

            /**
             * \brief Changes the palette.
             * \param pal Asked palette identifier.
             *
             * The palettes set used to be allocated on the stack but I moved
             * it to the heap so I could dump the four frame-buffers and
             * follow how frames are generated.
            */
            void changePal (std::uint8_t pal);

            /**
             * \brief Set the displayed page.
             * \param page Identifier of the video buffer to be displayed.
             */
            void updateDisplay (std::uint8_t page);

        private:
            /// \brief Type for drawing functions.
            typedef void (Video::*drawLine) (std::int16_t x1, std::int16_t x2,
                                             std::uint8_t col);

            /// \brief Size for video buffer.
            enum {VID_PAGE_SIZE = 320 * 200 / 2};

            /// \brief Size of the interpolation table.
            static const std::uint16_t interpTableSize = 0x400;

            /// \brief Number of framebuffer pages.
            static const std::size_t nPages = 4;

            /// \brief Game font definition.
            static const std::uint8_t _font [];

            /**
             * \brief Text for the English version.
             * \todo These should be read from executable file.
             */
            static const StrEntry _stringsTableEng [];

            /**
             * \brief Text for the English demo version.
             * \todo These should be read from executable file.
             */
            static const StrEntry _stringsTableDemo [];

            /// \brief Game resources.
            Resource* res;

            /// \brief System describer.
            SDLStub* sys;

            /// \brief Identifier for the palette requested.
            std::uint8_t paletteIdRequested;

            /// \brief Identifier of current palette.
            std::uint8_t currentPaletteId;

            /// \brief Video buffers represented linearly.
            std::uint8_t* frameBuffer;

            /// \brief List of video buffers.
            std::uint8_t* pages [nPages];

            /// \brief Working video buffer.
            std::uint8_t* curPagePtr1;

            /// \brief First background buffer.
            std::uint8_t* curPagePtr2;

            /// \brief Second background buffer.
            std::uint8_t* curPagePtr3;

            /// \brief Polygon describer.
            Polygon polygon;

            /// \brief To be completed ...
            std::int16_t hliney;

            /// \brief Precomputed division lookup table
            std::uint16_t interpTable [interpTableSize];

            /// \brief Pointer for accessing data in memory.
            Ptr pData;

            /// \brief Current data buffer.
            std::uint8_t* dataBuf;

            /**
             * \brief Draws a character.
             * \param c The character itself.
             * \param x Character abscissa.
             * \param y Character ordinate.
             * \param color Character colour.
             * \param buf Video buffer.
             */
            void drawChar (std::uint8_t c, std::uint16_t x, std::uint16_t y,
                           std::uint8_t color, std::uint8_t* buf);

            /**
             * \brief Draws a dot.
             * \param color Dot colour.
             * \param x Dot abscissa.
             * \param y Dot ordinate.
             */
            void drawPoint (std::uint8_t color, std::int16_t x,
                            std::int16_t y);

            /**
             * \brief Blends a line in the current framebuffer (curPagePtr1).
             * \param x1 To be completed ...
             * \param x2 To be completed ...
             * \param color To be completed ...
             */
            void drawLineBlend (std::int16_t x1, std::int16_t x2,
                                std::uint8_t color);

            /**
             * \brief To be completed ...
             * \param x1 To be completed ...
             * \param x2 To be completed ...
             * \param color To be completed 
             */
            void drawLineN (std::int16_t x1, std::int16_t x2,
                            std::uint8_t color);

            /**
             * \brief To be completed ...
             * \param x1 To be completed ...
             * \param x2 To be completed ...
             * \param color To be completed 
             */
            void drawLineP (std::int16_t x1, std::int16_t x2,
                            std::uint8_t color);
    };
}

#endif
