#ifndef __ENDIAN_HPP__
#define __ENDIAN_HPP__

/**
 * \file endian.hpp
 * \brief Managing endianness.
 * \author Le Bars, Yoann
 */

#include <boost/endian/conversion.hpp>

namespace AnotherWorld {
    /// \brief Type giving the endianness.
    enum Endian {littleEndian, bigEndian};

    /**
     * \brief Gives the endian of the computer on which the code is running.
     * \returns Native endianness.
     */
    inline Endian nativeEndianess () {
        /// Value to infer endianness.
        union {
            /// Value as a long integer.
            long l;
            /// Value as an array of characters.
            char c [sizeof(long)];
        } u;

        u.l = 1;

        if (u.c[sizeof(long) - 1] == 1) {
            return bigEndian;
        } else {
            return littleEndian;
        }
    }

    /**
     * \brief Convert endianness from the one of the file to native the native
     * one. In place version.
     * \param var Variable to convert.
     * \param endianness File endianness.
     */
    template <typename T>
    inline void file2NativeInplace (T &var, Endian endianness) {
        if (endianness == bigEndian) {
            boost::endian::big_to_native_inplace(var);
        } else {
            boost::endian::little_to_native_inplace(var);
        }
    }

    /**
     * \brief Convert endianness from the one of the file to native the native
     * one. Functional version.
     * \param var Variable to convert.
     * \param endianness File endianness.
     */
    template <typename T>
    inline T file2Native (const T &var, Endian endianness) {
        if (endianness == bigEndian) {
            return boost::endian::big_to_native(var);
        } else {
            return boost::endian::little_to_native(var);
        }
    }
}

#endif
