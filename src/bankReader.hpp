#ifndef __BANKREADER_HPP__
#define __BANKREADER_HPP__

/**
 * \file bankReader.hpp
 * \brief Definition of data banks for the game.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \author Willll
 * \author Glaize, Sylvain
 */

#include <boost/format.hpp>
#include <cstdint>
#include <string>

#include "endian.hpp"
#include "file.hpp"
#include "log.hpp"
#include "resources.hpp"

namespace AnotherWorld {
    /// \brief Descriptors to unpack data.
    struct UnpackContext {
        /**
         * \brief Cyclic redundancy check for the data. It is XOR'd with every
         * byte read from the packed buffer and must be zero at the end of the
         * unpacking.
         */
        std::uint32_t crc;

        /// \brief Latest 32 bit word read from the input stream.
        std::uint32_t currentWord;

        /**
         * \brief Size of data left to unpack. Must be zero at the end of the
         * unpacking.
         */
        std::uint32_t dataSize;
    };

    /**
     * \brief BankReader reads data from a bank file associated to a MemEntry
     * and loads it into a given preallocated buffer of the unpacked size of the
     * entry to load.
     *
     * The input data can be either packed or unpacked.
     *
     * If it is unpacked, it is simply loaded into the buffer.
     *
     * If it is packed, it is first loaded as is into the buffer then unpacked
     * in this same buffer. To allow using the same buffer, the data are
     * read from the end of the packed data and written to the end of the
     * buffer toward the beginning.
     *
     * This is based on the invariant that the packed data left to unpack is
     * always smaller than the space left by unpacked data.
     *
     */
    class BankReader {
        public:
            /**
             * \brief Reads a bank associated to the MemEntry and puts the content
             * into the pre-allocated buffer.
             * \param directory Directory where to find the bank.
             * \param me Memory entry descriptor.
             * \param buffer Working pre-allocated buffer which will contain the
             * unpack data at the end. This data must be allocated with a size
             * corresponding to the unpacked size of the entry.
             */
            BankReader (const std::string &directory, const MemEntry* me,
                        std::uint8_t* buffer);

            /**
             * \brief Gets the validity of the bank reading.
             * \return Whether or not reading procedure turned well.
             */
            [[nodiscard]] bool isValid () const {return bankIsValid;}

        private:
            /// \brief Context descriptor.
            UnpackContext unpackContext {};

            /**
             * \brief Input buffer pointer. Points to the next 32 bit word to read
             * from the pre-allocated buffer.
             */
            std::uint8_t* inputPointer;

            /**
             * \brief Output buffer pointer. Points to the next byte to write in
             * the pre-allocated buffer.
             */
            std::uint8_t* outputPointer {};

            /// \brief Pointer to the given pre-allocated buffer.
            std::uint8_t* bufferStart;

            /// \brief Endianess of the input data.
            Endian endianness;

            /**
             * \brief Validity of the read data. False if the bank file was not
             * found or if the unpack of data failed.
             * \todo Could be replaced by exceptions.
             */
            bool bankIsValid {true};

            /**
             * \brief Launch the unpack procedure.
             * \return Whether or not the procedure turned out well.
             */
            bool unpack ();

            /**
             * \brief Copy the next bytes from the input pointer to the output
             * pointer, unmodified.
             * \param lengthSize Bit length to read to obtain the length to
             * copy.
             * \param addCount Additional byte count to copy.
             */
            void copyNextBytes (std::uint8_t lengthSize, std::uint8_t addCount);

            /**
             * \brief Copy a sequence previously unpacked or copied into the
             * output buffer.
             * \param distanceSize Bit length to read to obtain the distance
             * from the current output pointer to find the sequence.
             * \param length of the sequence to copy.
             */
            void copyPreviousSequence (std::uint8_t distanceSize,
                                       std::uint16_t length);

            /**
             * \brief Gets a value from the input bitstream.
             * \param bitCount Numbers of bit to read to form the value.
             * \returns The read value.
             */
            std::uint16_t getBits (std::uint8_t bitCount);

            /**
             * \brief Gets the next bit from the input bit stream.
             * \returns The value of the read bit.
             */
            bool getNextBit ();

            /**
             * \brief Shift the current word read from the bit stream to the
             * right. This function is used by getNextBit() to acquire bits
             * from the bit stream. In case the current read word is 0, reads
             * a next word from the input stream.
             * \returns The initial bit 0 value.
             */
            bool getChunkBitAndShiftRight ();

            /**
             * \brief Reads a 32 bit word from the input buffer and sets the
             * input pointer to the next read position.
             * \returns The read value.
             */
            std::uint32_t readLongFromInput ();
    };
}

#endif
