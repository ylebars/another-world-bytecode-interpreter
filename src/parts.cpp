/**
 * \file parts.cpp
 * \brief Game parts description.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \author Willll
 * \author Glaize, Sylvain
 */

#include "parts.hpp"

#include <cassert>

/**
 * MEMLIST_PART_VIDEO1 and MEMLIST_PART_VIDEO2 are used to store polygons.
 *
 * It seems that:
 *
 * - MEMLIST_PART_VIDEO1 contains the cinematic polygons.
 * - MEMLIST_PART_VIDEO2 contains the polygons for player and enemies
 * animations.
 *
 * That would make sense since protection screen and cinematic game parts
 * do not load MEMLIST_PART_VIDEO2.
 */

namespace PartsPrivate {
    /// \brief The game is divided into 10 parts.
    const auto NUM_PARTS = 10;

    /// \brief Which kind of data.
    enum PositionOfIndex {
        PALETTE = 0,
        CODE = 1,
        POLY_CINEMATIC = 2,
        VIDEO2 = 3,
        INDEX_COUNT = 4,
    };

    /**
     * \brief Identifiers for resources used in the given part of the game.
     *
     * For each part of the game, four resources are referenced.
     * - MEMLIST_PART_PALETTE
     * - MEMLIST_PART_CODE
     * - MEMLIST_PART_VIDEO1
     * - MEMLIST_PART_VIDEO2
     * in that order
     */
    const std::uint8_t resourceIndicesForParts [NUM_PARTS][INDEX_COUNT] = {
        {0x14, 0x15, 0x16, 0x00},  // protection screens
        {0x17, 0x18, 0x19, 0x00},  // introduction cinematic
        {0x1A, 0x1B, 0x1C, 0x11},  //
        {0x1D, 0x1E, 0x1F, 0x11},  //
        {0x20, 0x21, 0x22, 0x11},  //
        {0x23, 0x24, 0x25, 0x00},  // battlechar cinematic
        {0x26, 0x27, 0x28, 0x11},  //
        {0x29, 0x2A, 0x2B, 0x11},  //
        {0x7D, 0x7E, 0x7F, 0x00},  //
        {0x7D, 0x7E, 0x7F, 0x00}   // password screen
    };
}

namespace AnotherWorld::Parts {
    PartResourceIndices getResourceIndices(Game::PartIdentifier partId) {
        assert(partId >= Game::PART_FIRST && partId <= Game::PART_LAST);
        /// Part index in the array.
        auto indexInArray = partId - Game::PART_FIRST;
        /// Resources indices for the given part.
        const auto& partIndices =
            PartsPrivate::resourceIndicesForParts[indexInArray];
        return {
            partIndices[PartsPrivate::PositionOfIndex::PALETTE],
            partIndices[PartsPrivate::PositionOfIndex::CODE],
            partIndices[PartsPrivate::PositionOfIndex::POLY_CINEMATIC],
            partIndices[PartsPrivate::PositionOfIndex::VIDEO2],
        };
    }
}
