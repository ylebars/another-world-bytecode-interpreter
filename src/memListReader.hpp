#ifndef __MEMLISTREADER_HPP__
#define __MEMLISTREADER_HPP__

/**
 * \file memListReader.hpp
 * \brief Definitions to manage game assets.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \author willll
 * \author Glaize, Sylvain
 */

#include <cstdint>
#include <string>

class MemEntry;

namespace AnotherWorld {
    class SignatureSeeker;

    /**
     * \brief Read game assets.
     * \param directory Directory where data are located.
     * \param memList Memory location to store data.
     * \param seeker An instance of a signature seeker.
     * \returns Number of entry read.
     * \throws UnlocatableFile
     */
    std::int16_t readEntriesFromFile(const std::string &directory,
                                     MemEntry* memList,
                                     const SignatureSeeker& seeker);
}

#endif
