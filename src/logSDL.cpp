/**
 * \file logSDL.cpp
 * \brief class to implement the logs through SDL_log
 * \author Willll
 * \author Le Bars, Yoann
 */

#include "logSDL.hpp"

#include <stdexcept>
#include <iostream>
#include <chrono>
#include <iomanip>
#include <boost/filesystem.hpp>

#include "errors.hpp"

namespace fs = boost::filesystem;

AnotherWorld::LogSDL::~LogSDL () {
    if (configuration.isFileEnable()) {
        // Close file handle
        ofs.close();
    }
}

void AnotherWorld::LogSDL::setup (const LoggerConfiguration &_configuration) {
    configuration = _configuration;
    // Disable console logging if not explicitly asked from CLI interface.
    if (!configuration.isConsoleEnable() && !configuration.isFileEnable()) {
        isSet = false;
    }
    else {
        if (configuration.isFileEnable()) {
            /// Path to the logging file.
            boost::filesystem::path logFile =
                boost::filesystem::current_path()
                    / configuration.getLogFilename();
            ofs.open(logFile);

            if (!ofs.is_open()) {
                configuration.resetFileEnable();
                if (!configuration.isConsoleEnable()) {
                    isSet = false;
                }
            }
        }

        SDL_LogSetOutputFunction(&fileLogger, this);
        SDL_LogSetPriority(SDL_LOG_CATEGORY_APPLICATION,
                           toSDLLogLevel(configuration.getSeverityLevel()));
    }
}

void AnotherWorld::LogSDL::log (SeverityLevels slvl, const char* format, ...) {
    if (isSet && slvl >= configuration.getSeverityLevel()) {
        /// Command line parameters.
        std::va_list va;
        va_start(va, format);
        log(slvl, format, va);
        va_end(va);
    }
}

void AnotherWorld::LogSDL::debug (ILogger::DebugSource cm, const char* format,
                                  ...) {
    if (isSet && cm) {
        /// Command line parameters.
        std::va_list va;
        va_start(va, format);
        log(SeverityLevels::DEBUG, format, va);
        va_end(va);

    }
}

void AnotherWorld::LogSDL::information (const char* format, ...) {
    if (isSet) {
        /// Command line parameters.
        std::va_list va;
        va_start(va, format);
        log(SeverityLevels::INFORMATION, format, va);
        va_end(va);
    }
}

void AnotherWorld::LogSDL::warning (const char* format, ...) {
    if (isSet) {
        /// Command line parameters.
        std::va_list va;
        va_start(va, format);
        log(SeverityLevels::WARNING, format, va);
        va_end(va);
    }
}

void AnotherWorld::LogSDL::error (const char* format, ...) {
    if (isSet) {
        /// Command line parameters.
        std::va_list va;
        va_start(va, format);
        log(SeverityLevels::ERROR, format, va);
        /// Message maximum size.
        const size_t maxSize = 1024;
        /// Error message.
        char message [maxSize];
        std::vsnprintf(message, maxSize, format, va);
        va_end(va);
        throw UnrecoverableError (message);
    }
}

SDL_LogPriority AnotherWorld::LogSDL::toSDLLogLevel (const SeverityLevels &severityLevel) const {
    switch (severityLevel) {
        case DEBUG:
            return SDL_LOG_PRIORITY_DEBUG;
        case INFORMATION:
            return SDL_LOG_PRIORITY_INFO;
        case WARNING:
            return SDL_LOG_PRIORITY_WARN;
        case ERROR:
            return SDL_LOG_PRIORITY_ERROR;
        case DISABLE:
        default:
            throw InvalidSeverityLevel ("toSDLLogLevel: Invalid severity level.");
    }
}

void AnotherWorld::LogSDL::log (SeverityLevels slvl, const char *format,
                                std::va_list va) {
    if (isSet && slvl >= configuration.getSeverityLevel()) {
        SDL_LogMessageV(SDL_LOG_CATEGORY_APPLICATION, toSDLLogLevel(slvl),
                        format, va);
    }
}

AnotherWorld::LoggerConfiguration &AnotherWorld::LogSDL::getLoggerConfiguration() {
    return configuration;
}

fs::ofstream & AnotherWorld::LogSDL::getFileHandler() {
    return ofs;
}

void AnotherWorld::LogSDL::fileLogger (void* userdata, int category,
                                       SDL_LogPriority priority,
                                       const char* message) {
    /// Data describer.
    auto* me = static_cast<LogSDL *>(userdata);
    /// Logging configuration describer.
    auto &configuration = me->getLoggerConfiguration();
    /// Logging file describer.
    auto &ofs = me->getFileHandler();

    /// Current time.
    const std::chrono::time_point<std::chrono::system_clock> now =
            std::chrono::system_clock::now();
    /// Current milliseconds.
    const auto ms = std::chrono::time_point_cast<std::chrono::milliseconds>(now).time_since_epoch().count() % 1000;
    /// Current time is standard form.
    const std::time_t t_c = std::chrono::system_clock::to_time_t(now);

    if (configuration.isConsoleEnable()) {
        std::cout << std::put_time(std::localtime(&t_c), "%F %T.")
                  << std::setfill('0') << std::setw(3) << ms
                  << std::put_time(std::localtime(&t_c), " %Z: ")
                  << message << std::endl;
    }
    if (configuration.isFileEnable()) {
        if(ofs.is_open()) {
            ofs << std::put_time(std::localtime(&t_c), "%F %T.")
                << std::setfill('0') << std::setw(3) << ms
                << std::put_time(std::localtime(&t_c), " %Z: ")
                << message << std::endl;
        } else {
            // File is closed, no point to keep logging into file,
            configuration.resetFileEnable();
        }
    }
}
