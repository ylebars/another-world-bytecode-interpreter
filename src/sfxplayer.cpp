/**
 * \file sfxplayer.cpp
 * \brief Methods to play game sound effects.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \author Glaize, Sylvain
 */

#include "sfxplayer.hpp"

#include <cassert>
#include <cstdint>
#include <cstring>

#include "log.hpp"
#include "mixer.hpp"
#include "resources.hpp"
#include "sys.hpp"

/// \brief Namespace for private function used in this particular compile unit.
namespace AnotherWorld::Private {
    /// \brief Value used to increase volume.
    const std::uint8_t EFFECT_VOLUME_UP = 5;
    /// \brief value used to decrease volume.
    const std::uint8_t EFFECT_VOLUME_DOWN = 6;

    /**
     * \brief Determines the actual volume in which play sound.
     * \param referenceVolume Absolute volume in sound design.
     * \param adjustment Volume adjustment the player asked for.
     * \return The actual volume to be played.
     */
    std::int32_t adjustNoteVolume (std::uint8_t referenceVolume,
                                   const std::uint16_t adjustment) {
        /// Resultant volume.
        auto computedVolume = static_cast<std::int32_t>(referenceVolume);

        /// To be completed ...
        std::uint8_t effect = (adjustment & 0x0F00) >> 8;
        if (effect == Private::EFFECT_VOLUME_UP) {
            /// Requested volume for the effect on the data.
            std::uint8_t volume = adjustment & 0xFF;
            computedVolume = std::min(computedVolume + volume, 0x3F);
        } else if (effect == Private::EFFECT_VOLUME_DOWN) {
            /// Requested volume for the effect on the data.
            std::uint8_t volume = (adjustment & 0xFF);
            computedVolume = std::max(computedVolume - volume, 0x00);
        }
        assert(computedVolume <= 0x3F);
        assert(computedVolume >= 0x00);
        return computedVolume;
    }
}

void AnotherWorld::SfxPlayer::setEventsDelay (std::uint16_t _delay) {
    Logger::getLogger().debug(ILogger::DBG_SND,
                              "SfxPlayer::setEventsDelay(%d)", _delay);
    /// Mutex for synchronisation.
    std::scoped_lock mutexLock {mutex};
    delay = _delay * 60 / 7050;
}

void AnotherWorld::SfxPlayer::loadSfxModule (std::uint16_t resourceIndex,
                                             std::uint16_t _delay,
                                             std::uint8_t pos) {
    Logger::getLogger().debug(ILogger::DBG_SND,
                              "SfxPlayer::loadSfxModule(0x%X, %d, %d)",
                              resourceIndex, _delay, pos);
    /// Mutex for synchronisation.
    std::scoped_lock mutexLock {mutex};

    /// Resource describer.
    MemEntry& me = res->memList[resourceIndex];

    if (me.state == MemState::STATE_LOADED && (me.type == Resource::RT_MUSIC)) {
        currentResource = resourceIndex;
        std::memset(&sfxMod, 0, sizeof(SfxModule));
        sfxMod.curOrder = pos;
        sfxMod.numOrder = *reinterpret_cast<std::uint8_t*>(me.bufPtr + 0x3E);
        Logger::getLogger().debug(
            ILogger::DBG_SND,
            "SfxPlayer::loadSfxModule() curOrder = 0x%X numOrder = 0x%X",
            sfxMod.curOrder, sfxMod.numOrder);
        for (std::size_t i = 0; i < 0x80; ++i) {
            sfxMod.orderTable[i] = *(me.bufPtr + 0x40 + i);
        }
        if (_delay == 0) {
            delay = file2Native(*reinterpret_cast<std::uint8_t*>(me.bufPtr),
                                me.fileEndianness);
        } else {
            delay = _delay;
        }
        delay = delay * 60 / 7050;
        sfxMod.data = me.bufPtr + 0xC0;
        Logger::getLogger().debug(
            ILogger::DBG_SND, "SfxPlayer::loadSfxModule() eventDelay = %d ms",
            delay);
        prepareInstruments(me.bufPtr + 2);
    } else {
        Logger::getLogger().warning("SfxPlayer::loadSfxModule() ec=0x%X", 0xF8);
    }
}

void AnotherWorld::SfxPlayer::prepareInstruments (const std::uint8_t* data) {
    std::memset(sfxMod.instruments, 0, sizeof(sfxMod.instruments));

    for (auto& instrument: sfxMod.instruments) {
        /// Instrument describer. To be completed ...
        const std::uint16_t resourceIndex = file2Native(
            *reinterpret_cast<const std::uint16_t*>(data), sys->fileEndian());
        data += 2;
        if (resourceIndex != 0) {
            instrument.volume = static_cast<std::uint8_t>(
                file2Native(*reinterpret_cast<const std::uint16_t*>(data),
                            sys->fileEndian()));
            /// Reference to assets in memory.
            MemEntry& me = res->memList[resourceIndex];
            if (me.state == MemState::STATE_LOADED &&
                me.type == Resource::RT_SOUND) {
                instrument.data = me.bufPtr;
                std::memset(instrument.data + 8, 0, 4);
                Logger::getLogger().debug(
                    ILogger::DBG_SND, "Loaded instrument 0x%X n=%d volume=%d",
                    resourceIndex,
                    std::distance(&instrument, sfxMod.instruments),
                    instrument.volume);
            } else {
                Logger::getLogger().error("Error loading instrument 0x%X",
                                          resourceIndex);
            }
        }
        data += 2;  // skip volume
    }
}

void AnotherWorld::SfxPlayer::start () {
    Logger::getLogger().debug(ILogger::DBG_SND, "SfxPlayer::start()");
    /// Mutex for synchronisation.
    std::scoped_lock mutexLock {mutex};
    sfxMod.curPos = 0;
    activeTimeId = sys->addTimer(delay, eventsCallback, this);
}

void AnotherWorld::SfxPlayer::stop () {
    Logger::getLogger().debug(ILogger::DBG_SND, "SfxPlayer::stop()");
    /// Mutex for synchronisation.
    std::scoped_lock mutexLock {mutex};
    if (currentResource != 0) {
        currentResource = 0;
        sys->removeTimer(activeTimeId);
    }
}

void AnotherWorld::SfxPlayer::handleEvents () {
    /// Mutex for synchronisation.
    std::scoped_lock mutexLock {mutex};
    /// To be completed ...
    std::uint8_t order = sfxMod.orderTable[sfxMod.curOrder];
    /// To be completed ...
    const std::uint8_t* patternData =
        sfxMod.data + sfxMod.curPos + order * 1024;
    for (std::uint8_t ch = 0; ch < 4; ++ch) {
        handlePattern(ch, patternData);
        patternData += 4;
    }
    sfxMod.curPos += 4 * 4;
    Logger::getLogger().debug(
        ILogger::DBG_SND,
        "SfxPlayer::handleEvents() order = 0x%X curPos = 0x%X", order,
        sfxMod.curPos);
    if (sfxMod.curPos >= 1024) {
        sfxMod.curPos = 0;
        order = sfxMod.curOrder + 1;
        if (order == sfxMod.numOrder) {
            currentResource = 0;
            sys->removeTimer(activeTimeId);
            mixer->stopAll();
        }
        sfxMod.curOrder = order;
    }
}

void AnotherWorld::SfxPlayer::handlePattern (std::uint8_t channel,
                                             const std::uint8_t* data) {
    /// Extract the current note/order and parameters.
    const auto* noteData = reinterpret_cast<const std::uint16_t*>(data + 0);
    /// First note for the sound effect.
    const auto note1 = file2Native(*noteData, sys->fileEndian());
    /// Second note for the sound effect.
    const auto note2 = file2Native(*(noteData + 1), sys->fileEndian());

    if (note1 == 0xFFFD) {
        Logger::getLogger().debug(
            ILogger::DBG_SND,
            "SfxPlayer::handlePattern() _scriptVars[0xF4] = 0x%X", note2);

        /**
         * \brief To be completed.
         * \todo It doesn't seem to be used. Used by other implementations?
         */
        *markVar = *reinterpret_cast<const std::int16_t*>(&note2);
    } else if (note1 == 0xFFFE) {
        mixer->stopChannel(channel);
    } else {
        const std::uint16_t instrumentIndex = (note2 & 0xF000) >> 12;

        if (instrumentIndex != 0) {
            /// Pointer to instrument data.
            const auto& instrument = sfxMod.instruments[instrumentIndex - 1];
            std::uint8_t* instrumentSampleData = instrument.data;

            if (instrumentSampleData != nullptr) {
                Logger::getLogger().debug(
                    ILogger::DBG_SND,
                    "SfxPlayer::handlePattern() preparing sample %d",
                    instrumentIndex);

                /// Requested volume.
                int32_t adjustedVolume =
                    Private::adjustNoteVolume(instrument.volume, note2);
                mixer->setChannelVolume(channel, adjustedVolume);

                if (note1 != 0) {
                    playNote(channel, note1, instrumentSampleData,
                             adjustedVolume);
                }
            }
        }
    }
}

void AnotherWorld::SfxPlayer::playNote (std::uint8_t channel,
                                        const std::uint16_t note,
                                        const std::uint8_t* instrumentSampleData,
                                        std::int32_t adjustedVolume) {
    /// Localisation on memory of the sample to be used.
    const auto* sampleData =
        reinterpret_cast<const uint16_t*>(instrumentSampleData);

    /// Sample data is after the two 16 bit parameters.
    const std::uint16_t SAMPLE_START = 8;

    /// Mixer describer.
    MixerChunk mc;
    mc.data = instrumentSampleData + SAMPLE_START;
    mc.len = file2Native(*sampleData, sys->fileEndian()) * 2;
    mc.loopLen = file2Native(*(sampleData + 1), sys->fileEndian()) * 2;
    if (mc.loopLen == 0) {
        mc.loopPos = 0;
    } else {
        mc.loopPos = mc.len;
    }

    assert(note >= 0x37 && note < 0x1000);

    /// Convert Amiga period value to hz.
    const std::uint16_t freq = 7159092 / (note * 2);
    Logger::getLogger().debug(
        ILogger::DBG_SND,
        "SfxPlayer::handlePattern() adding sample freq = 0x%X", freq);
    mixer->playChannel(channel, mc, freq, adjustedVolume);
}

std::uint32_t AnotherWorld::SfxPlayer::eventsCallback (std::uint32_t,
                                                       void* param) {
    /// To be completed/
    auto* p = reinterpret_cast<SfxPlayer*>(param);
    p->handleEvents();
    return p->delay;
}
