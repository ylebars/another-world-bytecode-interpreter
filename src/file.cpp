/**
 * \file file.cpp
 * \brief File handling implementation.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \author Glaize, Sylvain
 */

#include "file.hpp"

#include <boost/algorithm/string.hpp>

boost::filesystem::path AnotherWorld::getActualFilename (
    const boost::filesystem::path &filepath) {
    namespace fs = boost::filesystem;
    namespace algo = boost::algorithm;

    /// Possible name of the file containing game opcodes.
    const auto candidateFilename =
        algo::to_lower_copy(filepath.filename().string());

    /// Directory where to find game assets.
    const auto directory = filepath.parent_path();
    if (exists(directory) && is_directory(directory)) {
        for (const auto &it: boost::filesystem::directory_iterator(directory)) {
            /// File name to be tested.
            const auto concreteFilename =
                algo::to_lower_copy(it.path().filename().string());

            if (candidateFilename == concreteFilename) {
                return it.path();
            }
        }
    }

    return boost::filesystem::path();
}

bool AnotherWorld::openFile (File &file, const std::string &filename,
                             const std::string &directory,
                             Endian fileEndianness) {
    namespace fs = boost::filesystem;
    namespace algo = boost::algorithm;

    if (file.is_open()) {file.close();}

    // Searches for the actual path on the disk.
    /// Candidate file path name.
    auto path = fs::path{directory} / filename;
    /// Actual file path name.
    auto actualPath = getActualFilename(path);

    file.open(actualPath.string(), fileEndianness);

    return file.is_open();
}
