#ifndef __LOGSDL_HPP__
#define __LOGSDL_HPP__

/**
 * \file logSDL.hpp
 * \brief class to implement the logs through SDL_Log
 * \author Willll
 */

#include "ilog.hpp"

#include <cstdarg>
#include <cstdint>
#include <boost/filesystem/fstream.hpp>

#include <SDL_log.h>

namespace AnotherWorld {
    namespace fs = boost::filesystem;

    /// \brief Class for legacy logging.
    class LogSDL : public ILogger {
        public :
            /// \brief Default constructor disabled.
            LogSDL () = delete;
            /// \brief No default copy constructor.
            LogSDL(LogSDL const &) = delete;
            /// \brief No default move operator.
            LogSDL(LogSDL const &&) = delete;
            /// \brief No default assignment operator.
            LogSDL &operator = (LogSDL const &) = delete;

            /**
             * \brief Constructor.
             * \param isSet Whether or not logging.
             */
            explicit LogSDL (bool isSet): ILogger (isSet), configuration () {}

            /// \brief Destructor.
            virtual ~LogSDL ();

            /**
             * \brief Sending a log message.
             * \param slvl Message severity level.
             * \param format Message format describer.
             */
            void log (SeverityLevels slvl, const char* format, ...) override;

            /**
             * \brief Sending a debug message.
             * \param cm Code for message to generate.
             * \param format Message format describer.
             */
            void debug (ILogger::DebugSource cm, const char* format,
                        ...) override;

            /**
             * \brief Sending a warning message.
             * \param format Message format describer.
             */
            void information (const char* format, ...) override;

            /**
             * \brief Sending a warning message.
             * \param format Message format describer.
             */
            void warning (const char* format, ...) override;

            /**
             * \brief Sending an error message.
             * \param format Message format describer.
             * \throws UnrecovarableError
             */
            void error (const char* format, ...) override;

            /**
             * \brief Setup logger
             * \param configuration Logger configuration object.
             */
            void setup (const LoggerConfiguration &_configuration) override;

            /**
             * \brief SDL Log callback to handle logging to file
             * \param userdata Data to be transmitted in log.
             * \param category Error category of the message.
             * \param SDL_LogPriority Log priority (unused).
             * \param message Message to be logged.
             * \returns this casted as a void.
             * \throws fs::filesystem_error
             */
            static void fileLogger (void* userdata, int category,
                                    SDL_LogPriority priority,
                                    const char* message);

            /**
             * \brief configuration getter
             * \returns reference to log configuration
             */
            LoggerConfiguration &getLoggerConfiguration ();

            /**
             * \brief File handler getter
             * \returns reference to the file handler
             */
            fs::ofstream &getFileHandler ();

        protected:
            /// \brief Logger configuration.
            LoggerConfiguration configuration;

            /// \brief ASCII file handler
            fs::ofstream ofs;

            /**
             * \brief Converts application log levels to SDL log level.
             * \param application Log level.
             * \returns SDL log level.
             * \throws InvalidSeverityLevel
             */
            inline SDL_LogPriority toSDLLogLevel (const SeverityLevels &severityLevel) const;

            /**
             * \brief Base for every log message.
             * \param slvl Security level for logging.
             * \param format Message format describer.
             * \param va Command line arguments.
             */
            void log (SeverityLevels slvl, const char* format, std::va_list va);
    };
}

#endif //__LOGSDL_HPP__
