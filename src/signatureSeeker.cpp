/**
 * \file signatureSeeker.cpp
 * \brief Searches for signature in a file.
 * \author Glaize, Sylvain
 * \author Le Bars, Yoann
 * \version 1.0
 */

#include "signatureSeeker.hpp"

#include <iostream>
#include <array>

#include "errors.hpp"
#include "file.hpp"
#include "log.hpp"

namespace AnotherWorld {
    /// Class to get signature from Amiga and Atari version.
    class LegacySignatureSeeker : public SignatureSeeker {
        public:
            explicit LegacySignatureSeeker (const char* filename):
                    filename{filename} {}

            /**
             * \brief Places the file at the beginning of the signature.
             * \param inputFile The file to searches the signature in.
             * \throws FailedSignatureReading
             * \throws NoValidSignature
             */
            void seek (File &inputFile) const override {
                /// Number of Entries composing the file signature.
                const std::size_t SIGNATURE_LENGTH = 20;
                /// Actual file signature.
                std::array<std::uint8_t, SIGNATURE_LENGTH> signature = {
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00, 0x1a, 0x3c, 0x00, 0x00, 0x1a, 0x3c,
                };

                /// Signature localisation.
                std::uint32_t signatureIndex = 0;
                /// Index offset in memory.
                std::uint32_t offset = 0;

                while (!inputFile.eof()) {
                    /// Current byte in file.
                    const std::uint8_t readByte =
                        inputFile.read<std::uint8_t>();
                    if (!inputFile.good() && !inputFile.eof()) {
                        /// Error message.
                        const char* message =
                            "An error occurred while reading \"%s\" at "
                            "offset %i\n";
                        Logger::getLogger().error(message, filename.c_str(),
                                                  offset);
                        throw FailedSignatureReading (filename);
                    }

                    offset += 1;
                    if (readByte == signature[signatureIndex]) {
                        signatureIndex += 1;
                        if (signatureIndex == SIGNATURE_LENGTH) {
                            break;
                        }
                    } else {
                        offset -= signatureIndex;
                        inputFile.seek(offset);
                        signatureIndex = 0;
                    }
                }

                if (signatureIndex != SIGNATURE_LENGTH) {
                    Logger::getLogger().error(
                        "Resource::readEntries() memList signature could not "
                        "be found in '%s' file\n",
                        filename.c_str());
                    throw NoValidSignature (filename);
                }

                // Gets back at first entry.
                // The signature searches for the second entry, so we're going
                // backward an additional time.
                offset -= SIGNATURE_LENGTH * 2;
                inputFile.seek(offset);
            }

            /**
             * \brief Gets the filename to search the signature in.
             * \return The filename to search the signature in.
             */
            [[nodiscard]] const std::string &getFilename () const override {
                return filename;
            }

        private:
            /// \brief Name of assets file.
            std::string filename;
    };

    /// \brief Class to get signature from MS-DOS version.
    class DosSignatureSeeker : public SignatureSeeker {
        public:
            /**
             * \brief Places the file at the beginning of the signature.
             * \param inputFile The file to searches the signature in.
             */
            void seek (File &inputFile) const override {}

            /**
             * \brief Gets the file name to search the signature in.
             * \return The file name to search the signature in.
             */
            [[nodiscard]] const std::string &getFilename () const override {
                return filename;
            }

        private:
            /// \brief Name of assets file.
            std::string filename {"memlist.bin"};
    };

    /// \brief Class to get signature from 20th anniversary edition.
    class TwentiethSignatureSeeker: public SignatureSeeker {
       public:
           /**
            * \brief Places the file at the beginning of the signature.
            * \param inputFile The file to searches the signature in.
            */
           void seek (File &inputFile) const override {}

           /**
            * \brief Gets the file name to search the signature in.
            * \return The file name to search the signature in.
            */
           [[nodiscard]] const std::string &getFilename () const override {
               return filename;
           }

       private:
           /// \brief Name of assets file.
           std::string filename {""};
    };

    std::unique_ptr<SignatureSeeker> getSignatureSeeker (DataType dataType) {
        switch (dataType) {
            case DT_DOS: {
                return std::make_unique<DosSignatureSeeker>();
            }
            case DT_ATARI: {
                return std::make_unique<LegacySignatureSeeker>("START.PRG");
            }
            case DT_AMIGA: {
                return std::make_unique<LegacySignatureSeeker>("another");
            }
            default:
                Logger::getLogger().error(
                    "getSignatureSeeker() unsupported data type.");
                throw UnsupportedDataType ();
        }
    }
}
