#ifndef __RAW_TITLEDETECTION_HPP__
#define __RAW_TITLEDETECTION_HPP__

/**
 * \file titleDetector.hpp
 * \brief Determine which version of the game is available on the data
 * directory.
 * \author Glaize, Sylvain
 * \author Le Bars, Yoann
 */

#include <string>

namespace AnotherWorld {
    /// \brief Version descriptor
    enum DataType {
        DT_UNKNOWN,
        DT_15TH_EDITION,
        DT_20TH_EDITION,
        DT_DOS,
        DT_AMIGA,
        DT_ATARI,
        DT_WIN31,
        DT_3DO,
    };

    /// \brief Language descriptor.
    enum Language {
        LANG_FR,
        LANG_US,
        LANG_DE,
        LANG_ES,
        LANG_IT
    };

    /**
     * \brief Leverage the files found in the provided data path to
     * detect the game variant.
     *
     * From a given base path that describes either a directory or an archive,
     * it will run a series of checkers to determine which data type version
     * is contained in this directory or archive.
     *
     * From this data type and given a language, it also provides the game
     * title.
     */
    class TitleDetection {
        public:
            /**
             * \brief Constructor.
             * \param directory Name of the directory where to find files for
             * the game.
             */
            explicit TitleDetection (const std::string &directory);

            /**
             * \brief Access to type of data detected.
             * \returns Data type.
             */
            [[nodiscard]] DataType getDataType () const {return dataType;}

            /**
             * \brief Determine the name of the game.
             * \returns Game name.
             *
             * The game has been published with various names, depending of
             * the geographic area. In Europe and Australia, it was named
             * Another World, in Northern America it was named
             * Out of this world, and in Japan it was named Outer World.
             */
            [[nodiscard]] std::string getGameTitle (Language language) const;

        private:
            /// Type of accessed data.
            DataType dataType {DT_UNKNOWN};
    };

    /**
     * \brief Helper function that sends the title detection result to the debug
     * output.
     */
    void logsTitleDetection (const TitleDetection &titleDetection);
}

#endif
