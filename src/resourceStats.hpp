#ifndef __RESOURCESTATS_HPP__
#define __RESOURCESTATS_HPP__

/**
 * \file resourceStats.hpp
 * \brief File for handling stats of resources.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Glaize, Sylvain
 */

#include <array>
#include <cstdint>

#include "resources.hpp"

namespace AnotherWorld {
    /// \brief Class for handling statistics of resources.
    class ResourceStats {
        public:
            /**
             * \brief Declares a new resource that will count in the
             * statistics.
             * \param memEntry The memEntry containing the resource
             * information.
             */
            void addEntry(const MemEntry &memEntry);

            /// \brief Emits the summary of the statistics in the log.
            void logSummary();

        private:
            /// \brief Constants for indices in the stat arrays
            enum {UNCOMPRESSED_INDEX = 0, COMPRESSED_INDEX = 1};

            /**
             * \brief Indices for the index of the totals and the size of the
             * stat arrays.
             */
            enum {TOTAL_INDEX = 6, STAT_ARRAY_SIZE = 7};

            /**
             * \brief Handles the total size (compressed and uncompress) for
             * each type of resource, the total is in the latest array
             * position.
             */
            std::array<std::array<int, 2>, STAT_ARRAY_SIZE> resourceSizeStats {};

            /**
             * \brief Handles the total count (compressed and uncompress) for
             * each type of resource, the total is in the latest array
             * position.
             */
            std::array<std::array<int, 2>, STAT_ARRAY_SIZE> resourceUnitStats {};

            /// \brief Count of resources declared to this stat object.
            std::uint32_t resourceCount = 0;

            /// \brief Emits the the statistics for a specific resource in the log.
            void logEntry (const MemEntry &memEntry) const;
    };
}

#endif
