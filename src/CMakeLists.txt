## Project compilation.

if((${CMAKE_SYSTEM_NAME} MATCHES "Windows") AND (CMAKE_CXX_COMPILER_ID MATCHES "MSVC"))
    set(EXECUTABLE_OUTPUT_PATH bin)
else()
    # Path for the executable.
    set(EXECUTABLE_OUTPUT_PATH bin/${CMAKE_BUILD_TYPE})
endif()

configure_file(
    "${PROJECT_SOURCE_DIR}/src/config.hpp.in"
    "${PROJECT_BINARY_DIR}/config.hpp"
)
include_directories(${PROJECT_BINARY_DIR})

# List of source files.
set(
    SOURCE_FILES
    ilog.hpp
    log.hpp
    log.cpp
    logSDL.hpp
    logSDL.cpp
    errors.hpp
    errors.cpp
    bankReader.hpp
    bankReader.cpp
    endian.hpp
    engine.hpp
    engine.cpp
    file.hpp
    file.cpp
    intern.hpp
    main.cpp
    mixer.hpp
    mixer.cpp
    parts.hpp
    parts.cpp
    resources.hpp
    resources.cpp
    sfxplayer.hpp
    sfxplayer.cpp
    staticres.cpp
    sys.hpp
    sys.cpp
    video.hpp
    video.cpp
    vm.hpp
    vm.cpp
    titleDetector.hpp
    titleDetector.cpp
    vmMemoryAllocator.hpp
    vmMemoryAllocator.cpp
    resourceStats.hpp
    resourceStats.cpp
    memListReader.hpp
    memListReader.cpp
    controls/icontroller.hpp
    controls/keyboard.hpp
    controls/keyboard.cpp
    controls/joystick.hpp
    controls/joystick.cpp
    signatureSeeker.hpp
    signatureSeeker.cpp
)

add_executable(
    ${EXECUTABLE_NAME}
    ${SOURCE_FILES}
)

if(USE_CONAN)
    include_directories(${CMAKE_CURRENT_BINARY_DIR})
    target_link_libraries(${EXECUTABLE_NAME})
    conan_set_find_library_paths(${PROJECT_NAME})
    conan_target_link_libraries(${PROJECT_NAME})
else()
    find_package(SDL2 REQUIRED)
    find_package(
        Boost
        1.58.0
        REQUIRED COMPONENTS
        program_options
        filesystem
    )

    link_directories(${Boost_LIBRARY_DIRS})

    include_directories(
        ${CMAKE_CURRENT_BINARY_DIR}
        ${SDL2_INCLUDE_DIRS}
        ${Boost_INCLUDE_DIRS}
    )

    target_link_libraries(
        ${EXECUTABLE_NAME}
        ${SDL2_LIBRARIES}
        ${Boost_LIBRARIES}
    )
endif()

if((${CMAKE_SYSTEM_NAME} MATCHES "Windows") AND (CMAKE_CXX_COMPILER_ID MATCHES "MSVC"))
    configure_file(
        ${PROJECT_SOURCE_DIR}/resources/logo-aw.bmp
        ${EXECUTABLE_OUTPUT_PATH}/${CMAKE_BUILD_TYPE}/logo-aw.bmp
        COPYONLY
    )
    configure_file(
            ${PROJECT_SOURCE_DIR}/resources/gamecontrollerdb.txt
            ${EXECUTABLE_OUTPUT_PATH}/${CMAKE_BUILD_TYPE}/gamecontrollerdb.txt
            COPYONLY
    )
else()
    configure_file(
        ${PROJECT_SOURCE_DIR}/resources/logo-aw.bmp
        ${EXECUTABLE_OUTPUT_PATH}/logo-aw.bmp
        COPYONLY
    )
    configure_file(
            ${PROJECT_SOURCE_DIR}/resources/gamecontrollerdb.txt
            ${EXECUTABLE_OUTPUT_PATH}/gamecontrollerdb.txt
            COPYONLY
    )
endif()

if(CMAKE_BUILD_TYPE STREQUAL "Release")
    set_target_properties(${EXECUTABLE_NAME} PROPERTIES INTERPROCEDURAL_OPTIMIZATION TRUE)
endif()
