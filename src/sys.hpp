#ifndef __SYS_HPP__
#define __SYS_HPP__

/**
 * \file sys.hpp
 * \brief Description of the system on which the game is running.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \author Glaize, Sylvain
 * \author willl
 */

#include <unordered_map>
#include <cstdint>
#include <string>

// Note : SDL_Joystick refuses to be forward declared!
#include <SDL.h>

#include "controls/icontroller.hpp"
#include "endian.hpp"

namespace AnotherWorld {
    /// \brief Maximum number of colour in palette.
    const uint8_t ColorsMaxRender = 16;

    /// \brief Describing an entry from the player.
    struct PlayerInput {
        enum {
            DIR_LEFT = 1 << 0,
            DIR_RIGHT = 1 << 1,
            DIR_UP = 1 << 2,
            DIR_DOWN = 1 << 3
        };

        /// \brief Mask indicating which direction is required.
        std::uint8_t dirMask;

        /// \brief Whether or not the action button is pressed.
        bool button;

        /// \brief Whether or not the code screen is asked.
        bool code;

        /// \brief Whether or not the game must be paused.
        bool pause;

        /// \brief Whether or not to quit the game.
        bool quit;

        /// \brief Whether or not mute the game.
        bool mute;

        /// \brief Whether or not turn sound volume up.
        bool volumeUP;

        /// \brief Whether or not turn sound volume down.
        bool volumeDOWN;

        /// \brief To be completed ...
        char lastChar;
    };

    /// \brief System describer.
    class SDLStub {
        public:
            /// \brief Screen and sound constants.
            enum {
                SCREEN_W = 320,
                SCREEN_H = 200,
                SOUND_SAMPLE_RATE = 22050
            };

            /// \brief Default screen scale.
            const std::uint8_t DEFAULT_SCALE = 3;

            /// \brief Type describing audio functions.
            typedef void (*AudioCallback)(void* param, std::uint8_t* stream,
                                          int len);

            /// \brief Type describing timer functions.
            typedef uint32_t (*TimerCallback)(uint32_t delay, void* param);

            /// \brief Screen scale.
            std::uint8_t scale = DEFAULT_SCALE;

            /// \brief Whether or not the game should be in full screen.
            bool fullscreen = false;

            /// \brief Input from the player.
            PlayerInput input;

            /**
             * \brief Constructor.
             * \param _executablePath Path to the executable.
             */
            explicit SDLStub (char* _executablePath):
                    input (), executablePath (_executablePath) {}

            /**
             * \brief Copy constructor.
             * \param other Object to be copied.
             */
            SDLStub (const SDLStub &other):
                    scale (other.scale), fullscreen (other.fullscreen),
                    input (other.input), controllers (other.controllers),
                    screen (other.screen), window (other.window),
                    renderer (other.renderer), icon (other.icon),
                    fileEndian_ (other.fileEndian_),
                    executablePath (other.executablePath),
                    gameTitle (other.gameTitle) {}

            /**
             * \brief Move constructor.
             * \param other Object to be copied.
             */
            SDLStub (SDLStub &&other):
                    scale (other.scale), fullscreen (other.fullscreen),
                    input (other.input), controllers (other.controllers),
                    screen (other.screen), window (other.window),
                    renderer (other.renderer), icon (other.icon),
                    fileEndian_ (other.fileEndian_),
                    executablePath (other.executablePath),
                    gameTitle (other.gameTitle) {
                other.screen = nullptr;
                other.window = nullptr;
                other.renderer = nullptr;
                other.icon = nullptr;
            }

            /// \brief Destructor.
            ~SDLStub();

            /**
             * \brief Copy assignment operator.
             * \param other Object to be copied.
             * \note THIS LOOKS LIKE SOMETHING THAT WE DON'T WANNA SEE CALLED !
             */
            SDLStub &operator = (const SDLStub &other) {
                if (&other != this) {
                    cleanupGfxMode();
                    for (auto controller: controllers) {
                        controller.second->close();
                        delete controller.second;
                        controller.second = nullptr;
                    }

                    scale = other.scale;
                    fullscreen = other.fullscreen;
                    input = other.input;
                    controllers = other.controllers;
                    screen = other.screen;
                    window = other.window;
                    renderer = other.renderer;
                    icon = other.icon;
                    fileEndian_ = other.fileEndian_;
                    executablePath = other.executablePath;
                    gameTitle = other.gameTitle;
                }

                return *this;
            }

            /**
             * \brief Move assignment operator.
             * \param other Object to be copied.
             */
            SDLStub &operator = (SDLStub &&other) {
                if (&other != this) {
                    cleanupGfxMode();
                    for (auto controller: controllers) {
                        controller.second->close();
                        delete controller.second;
                        controller.second = nullptr;
                    }

                    scale = other.scale;
                    fullscreen = other.fullscreen;
                    input = other.input;
                    controllers = other.controllers;
                    screen = other.screen;
                    window = other.window;
                    renderer = other.renderer;
                    icon = other.icon;
                    fileEndian_ = other.fileEndian_;
                    executablePath = other.executablePath;
                    gameTitle = other.gameTitle;

                    other.screen = nullptr;
                    other.window = nullptr;
                    other.renderer = nullptr;
                    other.icon = nullptr;
                }

                return *this;
            }

            /**
             * \brief Indicates to what kind of endianness data files comply.
             * \returns Then endianness.
             */
            [[nodiscard]] Endian fileEndian () const {
                return fileEndian_;
            }

            /**
             * \brief Initialises system description.
             * \param gameTitle Game title.
             * \param _fileEndian To which endian game data files comply.
             */
            void init (const std::string& gameTitle, Endian _fileEndian);

            /**
             * \brief Sets current colour palette.
             * \param paletteBuffer Buffer to stock palette in memory.
             */
            void setPalette (const std::uint8_t* paletteBuffer);

            /**
             * \brief Updates game screen.
             * \param src Data to be set into video memory.
             */
            void updateDisplay (const std::uint8_t* src);

            /// \brief Handles player inputs.
            void processEvents ();

            /**
             * \brief Awaits for a certain duration.
             * \param duration How long to wait.
             */
            void sleep (std::uint32_t duration) {SDL_Delay(duration);}

            /**
             * \brief Gets current time stamp.
             * \returns The stamp.
             */
            std::uint32_t getTimeStamp () {return SDL_GetTicks();}

            /**
             * \brief Starts game audio.
             * \param callback Audio callback.
             * \param param Audio parameters.
             */
            void startAudio (AudioCallback callback, void* param);

            /// \brief Stops game audio.
            void stopAudio () {SDL_CloseAudio();}

            /**
             * \brief Gets output sample rate.
             * \returns The sample rate.
             */
            std::uint32_t getOutputSampleRate() {
                return SOUND_SAMPLE_RATE;
            }

            /**
             * \brief Adds some waiting timer.
             * \param delay Awaiting delay.
             * \param callback Timer callback.
             * \param param Timer parameters.
             * \returns Timer identifier.
             */
            int addTimer (std::uint32_t delay, TimerCallback callback,
                          void* param) {
                return SDL_AddTimer(delay, (SDL_TimerCallback) callback, param);
            }

            /**
             * \brief Removes a timer.
             * \param timerId Identifier of the timer to be removed.
             */
            void removeTimer (int timerId) {SDL_RemoveTimer(timerId);}

            /// \brief Prepares game window.
            void prepareGfxMode ();

            /// \brief Cleans game window.
            void cleanupGfxMode ();

            /// \brief Changes game window state.
            void switchGfxMode ();

        protected :
            /// \brief List of available controllers.
            std::unordered_map<Control::Type, Control::IController*> controllers;

        private:
            /// \brief Screen describer.
            SDL_Surface* screen = nullptr;

            /// \brief Game window describer.
            SDL_Window* window = nullptr;

            /// \brief Screen renderer describer.
            SDL_Renderer* renderer = nullptr;

            /// \brief Game icon describer.
            SDL_Surface* icon = nullptr;

            /// \brief Endianness to which data files comply.
            Endian fileEndian_ {};

            /// \brief The path to the executable.
            char* executablePath;

            /// \brief The Game Title
            std::string gameTitle;
    };
}

#endif // __SYS_HPP__
