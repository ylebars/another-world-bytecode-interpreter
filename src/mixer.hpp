#ifndef __MIXER_HPP__
#define __MIXER_HPP__

/**
 * \file mixer.hpp
 * \brief Definition for mixing sounds in the game.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \author Glaize, Sylvain
 */

#include <array>
#include <cstdint>
#include <mutex>
#include <thread>

#include "intern.hpp"

namespace AnotherWorld {
    // Forward declaration.
    class SDLStub;

    /// \brief Describe an audio element.
    struct MixerChunk {
        /// \brief Pointer to where the element is stored in memory.
        const std::uint8_t* data {};

        /// \brief Element duration.
        std::uint16_t len {};

        /// \brief Where does the loop start.
        std::uint16_t loopPos {};

        /// \brief Loop duration.
        std::uint16_t loopLen {};
    };

    /// \brief Audio mixer controller.
    struct MixerChannel {
        /// \brief Whether or not the channel is active.
        bool active {};

        /// \brief Sound volume requested by the user.
        std::uint8_t requestedVolume {};

        /// \brief Actual sound volume, result of general user volume and
        /// game sample volume.
        std::uint8_t volume {};

        /// \brief Element descriptor.
        MixerChunk chunk {};

        /**
         * \brief Position in memory of the next sample chunk to play combined
         * with an interpolation factor.
         *
         * The 8 lower bits consist of an advancement factor between the
         * actual offset in memory, which is shifted 8 bit left.
         */
        std::uint32_t positionInChunk {};

        /**
         * \brief How much to advance in positionInChunk, depending on the
         * frequency and play rate.
         */
        std::uint32_t positionIncrement {};
    };

    /// \brief Number of audio channels for the game.
    const std::uint8_t audioChannels = 4;

    /// \brief Describing an audio mixer for the game.
    class Mixer {
        public :
            /**
             * \brief Constructor.
             * \param stub System descriptor.
             */
            explicit Mixer (SDLStub* stub,  const uint8_t _volume);

            /**
             * \brief Copy constructor.
             * \param other The object to copy.
             */
           Mixer (const Mixer &other): sys (other.sys),
                                       volumeFactor (other.volumeFactor),
                                       channels (other.channels) {}

            /**
             * \brief Move constructor.
             * \param other Object to be moved.
             */
            Mixer (Mixer &&other): sys (other.sys),
                                   volumeFactor (other.volumeFactor),
                                   channels (other.channels) {
                other.sys = nullptr;
            }

            /// \brief Destructor.
            ~Mixer ();

            /**
             * \brief Copy assignment operator.
             * \param other Object to be copied.
             */
            Mixer &operator = (const Mixer &other) {
                if (&other != this) {
                    sys = other.sys;
                    volumeFactor = other.volumeFactor;
                    channels = other.channels;
                }

                return *this;
            }

            /**
             * \brief Move assignment operator.
             * \param other Object to be copied.
             */
            Mixer &operator = (Mixer &&other) {
                if (&other != this) {
                    sys = other.sys;
                    volumeFactor = other.volumeFactor;
                    channels = other.channels;
                    other.sys = nullptr;
                }

                return *this;
            }

            /**
             * \brief Playing sound on a given audio channel.
             * \param channel The channel to be played.
             * \param mixerChunk Element descriptor.
             * \param freq Sound frequency.
             * \param volume Sound volume.
             */
            void playChannel (std::uint8_t channel,
                              const MixerChunk& mixerChunk, std::uint16_t freq,
                              std::uint8_t volume);

            /**
             * \brief Stop a channel for playing sound.
             * \param channel Which channel to stop.
             */
            void stopChannel (std::uint8_t channel);

            /**
             * \brief Define volume for a given channel.
             * \param channel Which channel to deal with.
             * \param volume Sound volume to set.
             */
            void setChannelVolume (std::uint8_t channel, std::uint8_t volume);

            /// \brief Stop sound in all channels.
            void stopAll ();

            /**
             * \brief Mixing sounds.
             * \param buf Sound buffer.
             * \param len Sound length.
             *
             * This is SDL callback. Called in order to populate the buf with
             * len bytes. The mixer iterates through all active channels and
             * combine all sounds.
             *
             * Since there is no way to know when SDL will ask for a buffer
             * fill, we need to synchronize with a mutex so the channels remain
             * stable during the execution of this method.
             */
            void mix (std::int8_t* buf, int len);

            /**
             * \brief Callback for the mixer.
             * \param param Mixer parameters.
             * \param buf Sound buffer.
             * \param len Sound length.
             */
            static void mixCallback (void* param, std::uint8_t* buf, int len) {
                (reinterpret_cast<Mixer*>(param))->mix(reinterpret_cast<int8_t*>(buf), len);
            }

        private:
            /**
             * \brief Mutex for synchronisation with the virtual machine.
             *
             * Since the virtual machine and SDL are running simultaneously in
             * two different threads any read or write to an elements of the
             * sound channels MUST be synchronized with a mutex.
             */
            std::mutex mutex;

            /// \brief System descriptor.
            SDLStub* sys;

            /// \brief Percentage of sound volume with regards to the maximum.
            std::uint8_t volumeFactor;

            /// \brief Audio channels for the game.
            std::array<MixerChannel, audioChannels> channels {};

            /**
             * \brief Updates the volume of all channel according to the
             * user commands.
             */
            void updateUserVolume ();
    };
}

#endif
